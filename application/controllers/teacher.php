<?php
class Teacher extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function is_loggedin()
    {
        return $this->session->userdata('logged_in');
    }

    public function take_attendance()
    {
        if(!$this->is_loggedin())
        {
            $data['error'] = "Yor aren't logged in";
            $this->load->view('pages/index', $data);
        }
        else
        {
            $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';
            $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';
            $table_personal_info_teacher = $this->session->userdata('myschool_id') . '_personal_info_teacher';
            $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';
            $lid = $this->session->userdata('mylid');
            $query = $this->db->where('lid', $lid)->get($table_school_id_map);
            $data['my_l_info'] = $query->row_array();

            $query = $this->db
                        ->select('class_only_id, class_name')
                        ->distinct('class_only_id')
                        ->get($table_class_info_snapshot);
            $data['classes'] = $query->result();

            $data['userdata'] = $this->session->all_userdata();
            $data['variant'] = 'default';
            $data['highlight'] = 'My Home';
            // Now, we output the view
            $this->load->view('teacher/header', $data);
            $this->load->view('teacher/leftnav', $data);
            $this->load->view('teacher/take_attendance', $data);
            $this->load->view('common/footer', $data);
        }

    }

    public function ajax_attendance($func)
    {
        $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';
        $table_personal_info_teacher = $this->session->userdata('myschool_id') . '_personal_info_teacher';
        $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';

        if($func == 'class_list')
        {
            // Click on class_list, gives section list as output
            $class_only_id = $this->input->post('class_only_id');
            $query = $this->db
                ->where('class_only_id', $class_only_id)
                ->get($table_class_info_snapshot);
            $str = '<p>Select a section</p>';
            $str .= '<select id="section_list">';
            $str .= '<option>Please select a section:</option>';
            foreach($query->result() as $row)
            {
                $str .= '<option data-class_id="'.$row->class_id .'">';
                $str .= $row->section_name;
                $str .= '</option>';
            }
            $str .= '</select>';
            print($str);
        }

        if($func == 'section_list')
        {
            //Click on section_list. Gives students info as output
            $date = $this->input->post('date');
            $date = DateTime::createFromFormat('d-m-Y', $date);
            $timestamp = $date->getTimestamp();
            $day = date('z', $timestamp);
            $class_id = $this->input->post('class_id');
            $query = $this->db
                ->where('class_id', $class_id)
                ->get($table_school_id_map);
            // Check if attendance taken
            $q = $this->db->where('class_id', $class_id)->get($table_class_info_snapshot);
            $att_taken = $q->row()->attendance_taken;
            $att_taken_check = substr($att_taken, $day, 1);
            $str ="<p>Attendance taken on " .$this->input->post('date'). ": ";
            $str .= ($att_taken_check == '1'? 'Yes</p>' : 'No</p>');

            $str .= "<p>Students table</p>";
            $str .= "<table border=\"1\">";
            foreach($query->result() as $row)
            {
                $str .= '<tr>';
                $str .= '<td>' . $row->roll_number . '</td>';
                $str .= '<td>' . $row->f_name . ' ' . $row->s_name . '</td>';
                // Get the attendacnce of last 7 days
                //date begins at zero
                $attendance = $row->attendance;
                $attendance = substr($attendance, $day-7, 7);
                for($i=0; $i<7; $i++)
                {
                    $presence = ($attendance[$i]=='1'?'A':'P');
                    $str .= '<td class="att_'.$presence.'" data-lid="'.$row->lid.'">'.$presence.'</td>';
                }

                // Get today's attendance
                $today_att = substr($row->attendance, $day, 1);
                $presence = ($today_att=='1'?'A':'P');
                if($presence=='A')
                    $str .= '<td class="today selected att_'.$presence.'" data-lid="'.$row->lid.'">'.$presence.'</td>';
                else
                    $str .= '<td class="today att_'.$presence.'" data-lid="'.$row->lid.'">'.$presence.'</td>';
                $str .= '</tr>>';
            }
            $str .= '</table>';
            print($str);
        }
    }

    public function ajax_attendance_process()
    {
        $json = json_decode($this->input->post('json'), true);
        $date = $this->input->post('date');
        $date = DateTime::createFromFormat('d-m-Y', $date);
        $timestamp = $date->getTimestamp();
        $day_of_year = date('z', $timestamp) + 1;

        $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';
        $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';
        //print($table_class_info_snapshot);
        /*
        $json = array(
                '0002' => 'SixXAXXX_0002',
                '0003' => 'SixXAXXX_0003'
                );
        */

            print_r($json);
        foreach($json as $lid=>$a)
        {
            $class_id = substr($lid, 0, 8);

            // First update the string attendance_taken
            $query = $this->db->where('class_id', $class_id)->get($table_class_info_snapshot);
            $att_taken = $query->row()->attendance_taken;
            //print($att_taken);
            //print("<br/>");
            $new_att_taken = substr($att_taken, 0, $day_of_year-1) . '1' . substr($att_taken, $day_of_year);
            //print($new_att_taken);
            $arr = array(
                    'attendance_taken' => $new_att_taken
                    );
            $this->db
                ->where('class_id', $class_id)
                ->update($table_class_info_snapshot, $arr);
            // Now update the attendace string of the student

            $query = $this->db->where('lid', $lid)->get($table_school_id_map);
            $attendance = $query->row()->attendance;
            //print("<br> hello".$attendance."<br>");
            print($a);
            $new_attendance = substr($attendance, 0, $day_of_year-1) . $a . substr($attendance, $day_of_year);
            //print($new_attendance);
            $arr = array(
                    'attendance' => $new_attendance
                    );
            $this->db
                ->where('lid', $lid)
                ->update($table_school_id_map, $arr);
            //print("<br>One loop over <br>");
        }

        return 1;
    }

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */
