<?php
class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function is_loggedin()
    {
        return $this->session->userdata('logged_in');
    }

    public function home()
    {
        // Check if already logged in
        if(!$this->is_loggedin())
        {
            $data['error'] = "Yor aren't logged in";
            $this->load->view('pages/index', $data);
        }
        else
        {
            $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';
            $lid = $this->session->userdata('mylid');
            $query = $this->db->where('lid', $lid)->get($table_school_id_map);
            $data['my_l_info'] = $query->row_array();
            $data['userdata'] = $this->session->all_userdata();
            $data['variant'] = 'default';
            $data['highlight'] = 'My Home';
            // Now, we output the view
            $this->load->view('admin/header', $data);
            $this->load->view('admin/leftnav', $data);
            // Add a content view when data to display is decided
            // $this->load->view('admin/content', $data);
            $this->load->view('common/footer', $data);
        }
    }
    // function end: home

    public function initial_entry($todo='home')
    {
        if(!$this->is_loggedin())
        {
            $data['error'] = "Yor aren't logged in";
            $this->load->view('pages/index', $data);
        }
        else
        {
            $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';
            $table_personal_info_teacher = $this->session->userdata('myschool_id') . '_personal_info_teacher';
            $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';
            $lid = $this->session->userdata('mylid');
            $query = $this->db->where('lid', $lid)->get($table_school_id_map);

            $data['my_l_info'] = $query->row_array();
            $data['userdata'] = $this->session->all_userdata();

            if($todo == 'home')
            {
                $data['variant'] = 'default';
                $data['highlight'] = 'Initial Entry';

                // Just show the interface with changed left navigation
                $this->load->view('admin/header', $data);
                $this->load->view('admin/leftnav_init_entry', $data);
                $this->load->view('common/footer', $data);
            }

            if($todo == 'enter_principal')
            {
                $query = $this->db->select('f_name, m_name, s_name')->get($table_personal_info_teacher);
                $data['teachers'] = $query->result();
                $data['variant'] = 'default';
                $data['highlight'] = 'Enter Principal';
                if($this->input->server('REQUEST_METHOD') == 'POST')
                {
                    // Insert into three different tables repectively
                    // members, school_id_map, school_personal_info_teacher
                    // TODO : Port all db logic to model
                    $arr = array(
                        'cid' => $this->input->post('cid'),
                        'password' => $this->input->post('password'),
                        'lid' => $this->input->post('lid')
                    );
                    $this->db->insert('members', $arr);

                    $arr = array(
                        'lid' => $this->input->post('lid'),
                        'cid' => $this->input->post('cid'),
                        'f_name' => $this->input->post('f_name'),
                        's_name' => $this->input->post('s_name')
                    );
                    $this->db->insert($table_school_id_map, $arr);
                    $arr = array(
                        'f_name'=> $this->input->post('f_name'),
                        'm_name' => $this->input->post('m_name'),
                        's_name' => $this->input->post('s_name'),
                        'gender' => $this->input->post('gender'),
                        'dob' => $this->input->post('dob'),
                        'nationality' => $this->input->post('nationality'),
                        'blood_group' => $this->input->post('blood_grp'),
                        'father_name' => $this->input->post('father_name'),
                        'mother_name' => $this->input->post('mother_name'),
                        'phone_p' => $this->input->post('phone_p'),
                        'phone_s' => $this->input->post('phone_s'),
                        'email' => $this->input->post('email'),
                        'address_l1' => $this->input->post('address_l1'),
                        'address_l2_locality' => $this->input->post('address_l2_locality'),
                        'address_l3_city' => $this->input->post('address_l3_city'),
                        'address_pincode' => $this->input->post('address_pincode'),
                        'year_admit' => $this->input->post('year_admit')
                    );
                    $this->db->insert($table_personal_info_teacher, $arr);
                    $data['message'] = 'Input successful';
                }
                $this->load->view('admin/header', $data);
                $this->load->view('admin/leftnav_init_entry', $data);
                $this->load->view('admin/enter_content', $data);
                $this->load->view('common/footer', $data);
            } // Done: principal entry


            if($todo == 'enter_teachers')
            {
                $query = $this->db->select('f_name, m_name, s_name')->get($table_personal_info_teacher);
                $data['teachers'] = $query->result();
                $data['variant'] = 'default';
                $data['highlight'] = 'Enter Teachers';
                if($this->input->server('REQUEST_METHOD') == 'POST')
                {
                    // Insert into three different tables repectively
                    // members, school_id_map, school_personal_info_teacher
                    // TODO : Port all db logic to model
                    $arr = array(
                        'cid' => $this->input->post('cid'),
                        'password' => $this->input->post('password'),
                        'lid' => $this->input->post('lid')
                    );
                    $this->db->insert('members', $arr);

                    $arr = array(
                        'lid' => $this->input->post('lid'),
                        'cid' => $this->input->post('cid'),
                        'f_name' => $this->input->post('f_name'),
                        's_name' => $this->input->post('s_name')
                    );
                    $this->db->insert($table_school_id_map, $arr);
                    $arr = array(
                        'f_name'=> $this->input->post('f_name'),
                        'm_name' => $this->input->post('m_name'),
                        's_name' => $this->input->post('s_name'),
                        'gender' => $this->input->post('gender'),
                        'dob' => $this->input->post('dob'),
                        'nationality' => $this->input->post('nationality'),
                        'blood_group' => $this->input->post('blood_grp'),
                        'father_name' => $this->input->post('father_name'),
                        'mother_name' => $this->input->post('mother_name'),
                        'phone_p' => $this->input->post('phone_p'),
                        'phone_s' => $this->input->post('phone_s'),
                        'email' => $this->input->post('email'),
                        'address_l1' => $this->input->post('address_l1'),
                        'address_l2_locality' => $this->input->post('address_l2_locality'),
                        'address_l3_city' => $this->input->post('address_l3_city'),
                        'address_pincode' => $this->input->post('address_pincode'),
                        'year_admit' => $this->input->post('year_admit')
                    );
                    $this->db->insert($table_personal_info_teacher, $arr);
                    $data['message'] = 'Input successful';
                }
                $this->load->view('admin/header', $data);
                $this->load->view('admin/leftnav_init_entry', $data);
                $this->load->view('admin/enter_content', $data);
                $this->load->view('common/footer', $data);
            } // Done: teachers entry

            if($todo == 'enter_class')
            {
                // First we get the distinct class only ids
                // Then, we make an array corresponding to each
                // class_only_id with its sections
                $array_keys = array();
                $array_class_names = array();
                $query = $this->db
                            ->select('class_only_id, class_name')
                            ->distinct('class_only_id')
                            ->get($table_class_info_snapshot);
                foreach($query->result() as $row)
                {
                    $array_class_id[] = $row->class_only_id;
                    $array_class_names[] = $row->class_name;
                }

                // Now we make the array of values.
                // Each element is itself an array

                $array_sec_names = array();
                foreach($array_class_id as $id)
                {
                    $temp_array = array();
                    $query = $this->db
                                ->order_by('class_only_id')
                                ->where('class_only_id', $id)
                                ->get($table_class_info_snapshot);
                    foreach($query->result() as $row)
                    {
                        $temp_array[] = array(
                                            'sec_id' => $row->section_only_id,
                                            'sec_name' => $row->section_name
                                        );
                    }
                    $array_sec_names[] = $temp_array;
                    unset($temp_array);
                }

                // Now we have two arrays. One has the keys
                // corresponding to each class. Other is an array of
                // arrays which is a list of sections for a class
                // So, we combine these.
                $classid_section = array_combine($array_class_id, $array_sec_names);
                $classid_class_map = array_combine($array_class_id, $array_class_names);

                $data['classes'] = $query->result();
                $data['max_sections'] = 10; // yeah I know, it's arbit
                $data['variant'] = 'default';
                $data['highlight'] = 'Enter Classes';
                $data['classid_section'] = $classid_section;
                $data['classid_class_map'] = $classid_class_map;
                $this->load->view('admin/header', $data);
                $this->load->view('admin/leftnav_init_entry', $data);
                $this->load->view('admin/enter_class', $data);
                $this->load->view('common/footer', $data);
            } // Done: Enter class

            if($todo == 'enter_class_details')
            {
                $data['variant'] = 'default';
                $data['highlight'] = 'Initial Entry';

                $query = $this->db->order_by('class_id')->get($table_class_info_snapshot);
                $data['result'] = $query->result_array();

                $query = $this->db->get($table_personal_info_teacher);
                $data['result1'] = $query->result_array();

                // Just show the interface with changed left navigation
                $this->load->view('admin/header', $data);
                $this->load->view('admin/leftnav_init_entry', $data);
                $this->load->view('admin/enter_class_details', $data);
                $this->load->view('common/footer', $data);
            }

            if($todo == 'enter_subjects')
            {
                $data['variant'] = 'multiple_select';
                $data['highlight'] = 'Initial Entry';

                // Just show the interface with changed left navigation
                $this->load->view('admin/header', $data);
                $this->load->view('admin/leftnav_init_entry', $data);
                $this->load->view('admin/enter_subjects', $data);
                $this->load->view('common/footer', $data);
            }
        }
    }

    //temp function
    /*
    public function enter_class_process()
    {
        print("Tesing not done yet");
    }
    */

    public function enter_class_process()
    {
        // Get the json data sent via ajax and convert it to an array
        /* Explanation
         * ===========
         *
         * The json string will be of this form:
         * "{
         *      "10" : {},
         *      "NINT" : {
         *                  "className":"9th1",
         *                  "CX":"cs",
         *                  "sec3":"cd"
         *             },
         *      "Sixt" : {"className":"6th"},
         *      "TENT" : {},
         *      "Thre" : {},
         *      "newclass" : {}
         *  }"
         *  We loop over each top level key value pairs. The keys are original
         *  class ids. The values are arrays corresponding to each class_id.
         *  In the subarray, we again have key value pairs for each edited
         *  cell in the table. If in this subarray, a key is 'className', then
         *  the name of the class was changed. All other keys will be section
         *  ids and corresponding values will be section_names which have been
         *  changed. If a section id is of the form 'sec_<number>', then a new
         *  section is being created.
         *
         *  If a classid is 'newclass', then a new class is being created.
         */
        $json = json_decode(file_get_contents("php://input"), true);
        /*
        $json = array(
            '10'=> array(),
            'NINT' => array(),
            'Sixt' => array(),
            "TENT" => array(),
            "Thre" => array(),
            "newclass" => array(
                'className' => 'dfi',
                'sec1' => 'A',
                'sec2' => 'B'
                )
        );
        */
        $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';

        foreach($json as $class_only_id=>$sec_array)
        {
            // Each row corresponds to table cell that has been modified
            if($class_only_id != 'newclass')
            {
                // Here the class was already existing.
                // Now loop over all the entries in the subarray

                if(isset($sec_array['className']))
                {
                    // Check for class id conflict
                    $new_class_only_id = $this->generate_class_only_id($sec_array['className']);
                    if($new_class_only_id == 1062)
                    {
                        print("Class ".$sec_array['className']." already exists");
                        return 1;
                    }
                }

                foreach($sec_array as $s=>$entry)
                {
                    if(substr($s, 0, 3) == 'sec')
                    {
                        //print(substr($s, 0, 4));
                        // Create a section, name it and create a sec_id
                        // Creation of a new row in table
                        $class_name = $this->get_class_name($class_only_id);
                        $new_sec_id = $this->generate_section_only_id($class_name, $entry, $class_only_id);
                        if($new_sec_id == 1062)
                        {
                            print("Class $class_name, section $entry already exists");
                            return 1;
                        }
                        $classid = $this->generate_class_id($class_only_id, $new_sec_id);
                        $arr = array(
                                'class_name' => $class_name,
                                'class_id' => $classid,
                                'class_only_id' => $class_only_id,
                                'section_only_id' => $new_sec_id,
                                'section_name' => $entry
                                );
                        $this->db->insert($table_class_info_snapshot, $arr);
                    }
                    else if($s != 'className')
                    {
                        // Class is already there, section is there too,
                        // Just change the section name with sec_id
                        // Editing an old row in table
                        $class_name = $this->get_class_name($class_only_id);
                        $new_sec_id = $this->generate_section_only_id($class_name, $entry, $class_only_id);

                        if($new_sec_id == 1062)
                        {
                            print("Class $class_name, section $entry already exists");
                            return 1;
                        }
                        $arr = array(
                                'section_only_id' => $new_sec_id,
                                'section_name' => $entry
                                );
                        $where_arr = array(
                                'class_only_id' => $class_only_id,
                                'section_only_id' => $s
                                );
                        $this->db
                            ->where($where_arr)
                            ->update($table_class_info_snapshot, $arr);
                    }
                }
                // If we need to chenge the class id and class name as well
                if(isset($sec_array['className']))
                {
                    // Change class name and classid too
                    // Now that we have used the older class id to change info
                    // about sections, we can now change the class related info
                    $new_class_only_id = $this->generate_class_only_id($sec_array['className']);
                    $arr = array(
                        'class_name' => $sec_array['className'],
                        'class_only_id' => $new_class_only_id
                        );
                    $this->db
                        ->where('class_only_id', $class_only_id)
                        ->update($table_class_info_snapshot, $arr);
                }
                // Now, throughout the table, generate new class_ids which are
                // combination of class_only_id and section_only_id
                $this->update_classids();
            }
            else if($class_only_id == 'newclass' && isset($sec_array['className']))
            {
                // Create a new class
                // Obviously, since a new class is being created, therefore,
                // there should be a key names 'className' in the subarray.

                $new_class_only_id = $this->generate_class_only_id($sec_array['className']);
                if($new_class_only_id == 1062)
                {
                    print("Class ".$sec_array['className']."  already exists");
                    return 1;
                }

                foreach($sec_array as $s=>$entry)
                {
                    if(substr($s, 0, 3) == 'sec')
                    {
                        // Create a section, name it and create a sec_id
                        // Inerting a new row in table
                        $class_name = $sec_array['className'];
                        $new_sec_id = $this->generate_section_only_id($class_name, $entry, $new_class_only_id);
                        if($new_sec_id == 1062)
                        {
                            print("Class $class_name, section $entry already exists");
                            return 1;
                        }
                        $classid = $this->generate_class_id($new_class_only_id, $new_sec_id);
                        $arr = array(
                                'class_name' => $class_name,
                                'class_id' => $classid,
                                'class_only_id' => $new_class_only_id,
                                'section_only_id' => $new_sec_id,
                                'section_name' => $entry
                                );
                        print_r($arr);
                        $this->db->insert($table_class_info_snapshot, $arr);
                    }
                    else
                    {
                        print('Class cannot exist without a section');
                        return 1;
                        // Class cannot exist without a section, show error
                    }
                }
            }
        }
        //var_dump($json);
        // $data = file_get_contents("php://input");
        print("Data added successfully");
    }

    public function process_enter_class_details()
    {
        $max_input=$this->session->userdata('max_input');
        $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';
        for($i=1; $i<=$max_input; $i++)
        {
            $class_id=$this->input->post("class$i");
            $nstudent=$this->input->post("nstudent$i");
            $nteacher=$this->input->post("nteacher$i");
            $class_teacher=$this->input->post("class_teacher$i");
            $arr = array(
                    'num_student' => $nstudent,
                    'num_teacher' => $nteacher,
                    'class_teacher' => $class_teacher
                    );

            $this->db->where('class_id', $class_id)->update($table_class_info_snapshot, $arr);
        }
        redirect(base_url('admin/initial_entry/enter_class_details'));
    }

    public function ajax_class_list()
    {
        $table = $this->session->userdata('myschool_id') . '_class_info_snapshot';
        $q = $this->db
            ->select('class_id, class_name, section_name')
            ->like('class_name', $this->input->get('q'))
            ->get($table);
        $json_response = json_encode($q->result_array());
            //$json_response = $_GET["callback"] . "(" . $json_response . ")";
        echo $json_response;
    }

    public function enter_subjects_success()
    {
        $class_id=$this->input->post("class_id1");
        echo $class_id;
        $class_id=$this->input->post("class_id2");
        echo $class_id;
        $class_id=$this->input->post("class_id3");
        echo $class_id;
        $class_id=$this->input->post("class_id4");
        echo $class_id;
    }

    public function take_attendance()
    {
        if(!$this->is_loggedin())
        {
            $data['error'] = "You aren't logged in";
            $this->load->view('pages/index', $data);
        }
        else
        {
            $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';
            $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';
            $lid = $this->session->userdata('mylid');
            $query = $this->db->where('lid', $lid)->get($table_school_id_map);
            $my_l_info = $query->row_array();

            $array_keys = array();
            $array_class_names = array();
            $query = $this->db
                        ->select('class_only_id, class_name')
                        ->distinct('class_only_id')
                        ->get($table_class_info_snapshot);
            foreach($query->result() as $row)
            {
                $array_class_id[] = $row->class_only_id;
                $array_class_names[] = $row->class_name;
            }

            // Now we make the array of values.
            // Each element is itself an array

            $array_sec_names = array();
            foreach($array_class_id as $id)
            {
                $temp_array = array();
                $query = $this->db
                            ->order_by('class_only_id')
                            ->where('class_only_id', $id)
                            ->get($table_class_info_snapshot);
                foreach($query->result() as $row)
                {
                    $temp_array[] = array(
                                        'sec_id' => $row->section_only_id,
                                        'sec_name' => $row->section_name
                                    );
                }
                $array_sec_names[] = $temp_array;
                unset($temp_array);
            }

            // Now we have two arrays. One has the keys
            // corresponding to each class. Other is an array of
            // arrays which is a list of sections for a class
            // So, we combine these.
            $classid_section = array_combine($array_class_id, $array_sec_names);
            $classid_class_map = array_combine($array_class_id, $array_class_names);

            $data['classid_section'] = $classid_section;
            $data['classid_class_map'] = $classid_class_map;

            $data['my_l_info'] = $my_l_info;
            $data['userdata'] = $this->session->all_userdata();
            $data['variant'] = 'default';
            $data['highlight'] = 'Attendance';
            /*
            $sql4="select distinct substr(class_id,1,2) as class,class_name from $tbl5_name";
            $query = $this->db->query($sql4);
            $data['result'] = $query;

            $sql4="select class_id,class_name,section_name,last_attendance_update from $tbl5_name order by class_id";
            $query1 = $this->db->query($sql4);
            $data['result1'] = $query1;
            */

            // Now, we output the view
            $this->load->view('admin/header', $data);
            $this->load->view('admin/leftnav', $data);
            $this->load->view('admin/take_attendance', $data);
            $this->load->view('common/footer', $data);
        }

    }

    public function ajax_attendance_process()
    {
                $json = json_decode($this->input->post('json'), true);
                $map_json = json_decode($this->input->post('map_json'), true);
                $date = $this->input->post('date');
                $date = DateTime::createFromFormat('d-m-Y', $date);
                $timestamp = $date->getTimestamp();
                $day_of_year = date('z', $timestamp) + 1;

                $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';
                $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';
                //print($table_class_info_snapshot);
                /*
                $json = array(
                        '0002' => 'SixXAXXX_0002',
                        '0003' => 'SixXAXXX_0003'
                        );
                */
                foreach($json as $roll=>$lid)
                {
                    $class_id = substr($lid, 0, 8);
                    $a = $map_json[$lid];

                    // First update the string attendance_taken
                    $query = $this->db->where('class_id', $class_id)->get($table_class_info_snapshot);
                    $att_taken = $query->row()->attendance_taken;
                    //print($att_taken);
                    //print("<br/>");
                    $new_att_taken = substr($att_taken, 0, $day_of_year-1) . '1' . substr($att_taken, $day_of_year);
                    //print($new_att_taken);
                    $arr = array(
                            'attendance_taken' => $new_att_taken
                            );
                    $this->db
                        ->where('class_id', $class_id)
                        ->update($table_class_info_snapshot, $arr);
                    // Now update the attendace string of th student

                    $query = $this->db->where('lid', $lid)->get($table_school_id_map);
                    $attendance = $query->row()->attendance;
                    //print("<br> hello".$attendance."<br>");
                    $new_attendance = substr($attendance, 0, $day_of_year-1) . $a . substr($attendance, $day_of_year);
                    //print($new_attendance);
                    $arr = array(
                            'attendance' => $new_attendance
                            );
                    $this->db
                        ->where('lid', $lid)
                        ->update($table_school_id_map, $arr);
                    //print("<br>One loop over <br>");
                }

                return 1;
    }

    public function ajax_student_list()
    {
        $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';
        $table_personal_info_teacher = $this->session->userdata('myschool_id') . '_personal_info_teacher';
        $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';

        $date = $this->input->post('date');
        $date = DateTime::createFromFormat('d-m-Y', $date);
        $timestamp = $date->getTimestamp();
        $day = date('z', $timestamp);

        //$table = $this->session->userdata('myschool_id') . '_id_map';
        $class_id = $this->input->post('class_id');

        $q = $this->db->where('class_id', $class_id)->get($table_class_info_snapshot);
        $att_taken = $q->row()->attendance_taken;
        $att_taken_check = substr($att_taken, $day, 1);
        $str ="<p>Attendance taken on " .$this->input->post('date'). ": ";
        $str .= ($att_taken_check == '1'? 'Yes</p>' : 'No</p>');

        $str .= "<p>Roll number list</p>\n<table border=\"1\">\n<tr>";

        $query = $this->db
            ->where('class_id', $class_id)
            ->get($table_school_id_map);
        foreach($query->result() as $row)
        {
            $attendance = $row->attendance;
            $presence = ($attendance[$day]=='1'?'A':'P');
            if($presence=='A')
                $str .= '<td class="roll_number selected att_'.$presence.'" style="background-color:green;" data-lid="'. $row->lid .'">';
            else
                $str .= '<td class="roll_number att_'.$presence.'" style="background-color:green;" data-lid="'. $row->lid .'">';
            $str .= $row->roll_number;
            $str .= '</td>';
        }
        $str .= "</tr>\n</table>";
        print($str);
    }

/****************************** HELPER FUNCTIONS *****************************/
// Helper functions. Not to be called from outside

    public function generate_class_only_id($class_name)
    {
        // This is legacy code right here. I didn't change this function
        // when porting.
        $lookup_table = $this->session->userdata('myschool_id') . '_class_info_snapshot';
        $in1=strlen($class_name);
        if($in1==1)
            $append_text="XXX";
        else if($in1==2)
            $append_text="XX";
        else if($in1==3)
            $append_text="X";
        else $append_text = "";
        $i1=0;
        //$found = 0;

        while( $i1 <= $in1-3 ) // THIS LOOP GENERATES CLASS_ONLY_ID. IT DOES NOT YET WRITE BACK TO DATABASE. IT JUST CHECKS DATABASE AND COMPOSES A UNIQUE NEW CLASS_ONLY_ID
        {
            $num_matches = $this->db->where('class_name', $class_name)->count_all_results($lookup_table);
            if( $num_matches != 0 )
            {
                $found=1;
                break;
            }

            $class_only_id=substr($class_name,0,3).substr($class_name,$i1+3,1);
            $num_matches = $this->db->where('class_only_id', $class_only_id)->count_all_results($lookup_table);
            if( $num_matches == 0 )
            {
                $found=0;
                break;
            }
            else
            {
                $found=1;
            }
            $i1=$i1+1;
        }
        if($found==1)
        {
            return 1062;    //sql error code, :D
        }
        $class_only_id = $class_only_id.$append_text;
        return $class_only_id;
    }

    public function generate_section_only_id($class_name, $section_name, $class_only_id)
    {
        $lookup_table = $this->session->userdata('myschool_id') . '_class_info_snapshot';
        $in2=strlen($section_name);
        if($in2==1) $append_text="X";
        else $append_text = "";
        $i2=0;
        while( $i2 <= $in2-1 ) // THIS LOOP GENERATES SECTION_ONLY_ID. IT DOES NOT YET WRITE BACK TO DATABASE. IT JUST CHECKS DATABASE AND COMPOSES A UNIQUE NEW CLASS_ONLY_ID
        {
            $num_matches = $this->db
                                ->where(array(
                                            'section_name' => $section_name,
                                            'class_name' => $class_name
                                            )
                                       )
                                ->count_all_results($lookup_table);
            if( $num_matches != 0 )
            {
                $found=1;
                break;
            }
            $section_id=substr($section_name,0,1).substr($section_name,$i2+1,1);
            $num_matches = $this->db
                                ->where(array(
                                            'section_only_id' => $section_id,
                                            'class_only_id' => $class_only_id
                                            )
                                        )
                                ->count_all_results($lookup_table);
            if($num_matches == 0)
            {
                $found=0;
                break;
            }
            else
            {
                $found=1;
            }
            $i2=$i2+1;
        }
        if($found==1) //make it 1
        {
            return 1062;   //just a random code to represent repetition
        }
        $section_id=$section_id.$append_text;
        return($section_id);
    }

    public function generate_class_id($class_only_id, $section_only_id)
    {
        return $class_only_id . $section_only_id . "XX";
    }

    public function get_class_name($class_only_id)
    {
        $q = $this->db
                ->select('class_name')
                ->where('class_only_id', $class_only_id)
                ->get($this->session->userdata('myschool_id') . '_class_info_snapshot');
        return $q->row()->class_name;
    }

    public function update_classids()
    {
        $table = $this->session->userdata('myschool_id') . '_class_info_snapshot';
        $query = $this->db
                    ->select('class_id, class_only_id, section_only_id')
                    ->get($table);
        foreach($query->result() as $row)
        {
            $class_id = $this->generate_class_id($row->class_only_id, $row->section_only_id);
            $arr = array(
                    'class_id' => $class_id
                    );
            if($row->class_id != $class_id)
            {
                $this->db
                    ->where('class_id', $row->class_id)
                    ->update($table,$arr);
            }
        }
    }
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
