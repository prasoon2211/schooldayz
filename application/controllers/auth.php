<?php
class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function is_loggedin()
    {
        return $this->session->userdata('logged_in');
    }

    public function login()
    {
        // Check if already logged in
        if($this->is_loggedin())
        {
            $data['error'] = 'Yor are already logged in';
            $this->load->view('pages/index', $data);
        }

        // Loading libraries for form validation
        $this->load->helper('form');
        $this->load->library('form_validation');

        // Setting validation rules
        $this->form_validation->set_rules('myusername', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('mypassword', 'Password', 'trim|required|xss_clean');

        // Run validation on data
        // If fails, we load the homepage for logging in again
        // Error messages for validation will be displayed above the form
        // TODO : Use set_value() to generate submitted data on user side
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('pages/index');
        }

        // Form validated. Now login the user
        else
        {
            $cid = $this->input->post('myusername');
            $password = $this->input->post('mypassword');

            // Now authenticate
            $query = $this->db
                    ->where('cid', $cid)
                    ->where('password', $password)
                    ->limit(1)
                    ->get('members');
            if($query->num_rows() == 1)
            {
                // Success. Get data from db. Set session
                $result = $query->row();

                $arr = array(
                        'cid' => $result->cid,
                        'mylid' =>$result->lid,
                        'myr_s_lid_1' => $result->r_s_lid1,
                        'myr_s_lid_2' => $result->r_s_lid2,
                        'myr_s_lid_3' => $result->r_s_lid3,
                        'myr_s_lid_4' => $result->r_s_lid4,
                        'myr_s_lid_5' => $result->r_s_lid5,
                        'myr_s_lid_6' => $result->r_s_lid6,
                        'myr_s_lid_7' => $result->r_s_lid7,
                        'myr_s_lid_8' => $result->r_s_lid8,
                        'myr_t_lid_1' => $result->r_t_lid1,
                        'myr_t_lid_2' => $result->r_t_lid2,
                        'myr_t_lid_3' => $result->r_t_lid3,
                        'myr_t_lid_4' => $result->r_t_lid4,
                        'myr_t_lid_5' => $result->r_t_lid5,
                        'myr_t_lid_6' => $result->r_t_lid6,
                        'myr_t_lid_7' => $result->r_t_lid7,
                        'myr_t_lid_8' => $result->r_t_lid8,
                        'logged_in' => TRUE
                    );
                $this->session->set_userdata($arr);
                $myschool_id = substr($cid, 0, 4);
                $this->session->set_userdata('myschool_id', $myschool_id);

                // Get school details and set session
                $query = $this->db->where('school_id', $myschool_id)->get('school_details');
                $result = $query->row();

                $arr = array(
                        'myschool_name' => $result->school_name,
                        'myprincipal_name' => $result->principal_name,
                        'myschool_address' => $result->school_address,
                        'myschool_city' => $result->school_city
                    );
                $this->session->set_userdata($arr);

                // Redirect to the proper page
                $school_admin_cid = $result->school_id . '_admin';

                // Global admin login
                if($cid == 'admin')
                {
                    redirect(base_url('global_admin/home'));
                }
                else if($cid == $school_admin_cid)
                {
                    redirect(base_url('admin/home'));
                }
                // Add more cases for student, teacher interfaces etc.
                else
                {
                    redirect(base_url('student_home/login_success'));
                }
               

            }
            // Bad login credentials. Redirect with error message.
            else
            {
                $data['error'] = 'Username and/or password do not match.';
                $this->load->view('pages/index', $data);
            }
        }
    } // function end: login

    public function logout()
    {
        $this->session->sess_destroy();
    }
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */
