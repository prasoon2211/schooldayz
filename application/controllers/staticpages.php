<?php 
class Staticpages extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function pages($name='index')
    {
        if(! file_exists('application/views/pages/'.$name.'.php'))
        {
            show_404();
        }

        $data['title'] = ucfirst($name);
        $this->load->view("pages/$name", $data);
    }
}

/* End of file staticpages.php */
/* Location: ./application/controllers/staticpages.php */
