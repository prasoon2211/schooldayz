<?php
  
  Class Teacher_home extends CI_Controller
  {

  	public function __construct()
  	{
  		parent::__construct();
  		$this->load->helper('url');
      $this->load->helper('file');		
  	}

  	public function is_loggedin()
    {
        if($this->session->userdata('logged_in'))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function login_success()
    {

    	if($this->is_loggedin())
      {
         $temp_lid=$this->session->userdata('mylid');
         $temp_id_map=$this->session->userdata('myschool_id').'_id_map';
         $temp_individual_ann=$this->session->userdata('myschool_id').'_individual_ann';
         $temp_individual_msg=$this->session->userdata('myschool_id').'_individual_msg';
         $data=array();
         $result=array();
         $highlight['highlight']="My Home";
         $highlight['image_cid']=$this->session->userdata('cid');
        //Now get the data from _id_map
        $query=$this->db
                    ->where('lid',$temp_lid)
                    ->or_where('lid',$this->session->userdata('myr_s_lid_1'))->or_where('lid',$this->session->userdata('myr_s_lid_2'))
                    ->or_where('lid',$this->session->userdata('myr_s_lid_3'))->or_where('lid',$this->session->userdata('myr_s_lid_4'))
                    ->or_where('lid',$this->session->userdata('myr_s_lid_5'))->or_where('lid',$this->session->userdata('myr_s_lid_6'))
                    ->or_where('lid',$this->session->userdata('myr_s_lid_7'))->or_where('lid',$this->session->userdata('myr_s_lid_8'))
                    ->or_where('lid',$this->session->userdata('myr_t_lid_1'))->or_where('lid',$this->session->userdata('myr_t_lid_2'))
                    ->or_where('lid',$this->session->userdata('myr_t_lid_3'))->or_where('lid',$this->session->userdata('myr_t_lid_4'))
                    ->or_where('lid',$this->session->userdata('myr_t_lid_5'))->or_where('lid',$this->session->userdata('myr_t_lid_6'))
                    ->or_where('lid',$this->session->userdata('myr_t_lid_7'))->or_where('lid',$this->session->userdata('myr_t_lid_8'))
                    ->get($temp_id_map);

        $result_length=0;
        foreach($query->result() as $result1)
        {
            array_push($result, $result1);
            $result_length++;
            if($result1->lid==$temp_lid)
            $my_l_info=$result1;
        }
        $data['result']=$result;
        // Success. Get data from db. Set session
       //set to session variable because they need in more tables
       $arr=array(
           'myf_name' => $my_l_info->f_name,
           'mys_name' => $my_l_info->s_name,
           'mypost' => $my_l_info->post,
           'myattendance' => $my_l_info->attendance,
           'myunread_msg' => $my_l_info->unread_msg,
           'myunread_ann' => $my_l_info->unread_ann,
           'myevent_string' => $my_l_info->event_string,
           'myposition' => $my_l_info->position,
           'myann1' => $my_l_info->ann1,
           'myann2' => $my_l_info->ann2,
           'myann3' => $my_l_info->ann3,
           'myann4' => $my_l_info->ann4,
           'myann5' => $my_l_info->ann5,
           'mycal1' => $my_l_info->cal1,
           'mycal2' => $my_l_info->cal2,
           'mycal3' => $my_l_info->cal3,
           'mycal4' => $my_l_info->cal4,
           'mycal5' => $my_l_info->cal5
             );
           $this->session->set_userdata($arr);
           //display the announcement
           
           $data['t_announcement']=array();
           $data['t_ann_sender']=array();
           $data['t_ann_time']=array();
           $data['num_of_ann_msg']=array();
           $count=0;
           $query=$this->db
                       ->where('individual_lid',$temp_lid)
                       ->order_by('serial_number','desc')
                       ->get($temp_individual_ann);
            if($query->num_rows()>0)
            {
              foreach($query->result() as $result1)
              {
                $temp_announcements=$this->session->userdata('myschool_id').'_announcements'.$result1->serial_number;
                $temp_anns_string=$result1->received_ann_string;
                $i=strlen($temp_anns_string);
                $x=strlen($temp_anns_string);
                $var="";
                $check=0;

                while($i>0)
                {
                  if($temp_anns_string[$i-1]=='1')
                  {
                    if($check==0) {   $check=1;  }
                    else{ $var=$var." or "; }
                    $var=$var."position = '".($x-$i+1)."'";
                  }
                  $i--;
                }

                $query1=$this->db
                             ->where($var)
                             ->order_by('date_time_posted','desc')
                             ->limit(5-$count)
                             ->get($temp_announcements);

                $count=$count+$query1->num_rows();

                foreach($query1->result() as $result2)
                {
                    array_push($data['t_announcement'],$result2->ann_id);
                    array_push($data['t_ann_time'], $result2->date_time_posted);    
                    $query2=$this->db
                               ->select('f_name,s_name')
                               ->where('lid',$result2->sender_lid)
                               ->get($temp_id_map);
                    array_push($data['t_ann_sender'],$query2->row()->f_name);
                }
                if($count==5)
                {
                  break;
                }
              }
            }
            array_push($data['num_of_ann_msg'],$count);            

           //display the messages
           $data['t_message']=array();
           $data['t_msg_sender']=array();
           $data['t_msg_time']=array();
           $count=0;
           $query=$this->db
                       ->where('individual_lid',$temp_lid)
                       ->order_by('serial_number','desc')
                       ->get($temp_individual_msg);
            if($query->num_rows()>0)
            {
              foreach($query->result() as $result1)
              {
                $temp_messages=$this->session->userdata('myschool_id').'_message'.$result1->serial_number;
                $temp_msg_string=$result1->received_string;
                $i=strlen($temp_msg_string);
                $x=strlen($temp_msg_string);
                $var="";
                $check=0;

                while($i>0)
                {
                  if($temp_msg_string[$i-1]=='1')
                  {
                    if($check==0) {   $check=1;}
                    else{ $var=$var." or "; }
                    $var=$var."position = '".($x-$i+1)."'";
                  }
                  $i--;
                }

                $query1=$this->db
                             ->where($var)
                             ->order_by('date_time_posted','desc')
                             ->limit(5-$count)
                             ->get($temp_messages);

                $count=$count+$query1->num_rows();

                foreach($query1->result() as $result2)
                {
                    array_push($data['t_message'],$result2->msg_id);
                    array_push($data['t_msg_time'], $result2->date_time_posted);    
                    $query2=$this->db
                               ->select('f_name,s_name')
                               ->where('lid',$result2->sender_lid)
                               ->get($temp_id_map);
                    array_push($data['t_msg_sender'],$query2->row()->f_name);
                }
                if($count==5)
                {
                  break;
                }
              }
            }
           array_push($data['num_of_ann_msg'],$count); 
           //extracting and displaying classmates details
           array_push($data['num_of_ann_msg'],$result_length); 
           $this->load->view('teachers/T_header');
           $this->load->view('teachers/T_leftpanel',$highlight);
           $this->load->view('teachers/T_middle_home',$data);
           $this->load->view('teachers/footer');
        
      }
      else
      {
        $data['error'] = 'Yor are not logged in';
        redirect(base_url());
      }
    }

    public function student_all()
    {
      if($this->is_loggedin())
      {
        $temp_id_map=$this->session->userdata('myschool_id').'_id_map';
        $query=$this->db
                    ->select("substr(lid,1,3) as class",FALSE)
                    ->distinct('class')
                    ->not_like('substr(lid, 1, 3)','t','after')
                    ->get($temp_id_map);

        $data=array();
        $data['t_class']=array();
        $i=0;

        foreach($query->result() as $result1)
        {
           array_push($data['t_class'], $result1->class);
           $i++;
        }

        $data['num_of_class']=$i;
        $c_match=substr($this->session->userdata('mylid'),0,3);

        $query=$this->db
                    ->like('lid',$c_match,'after')
                    ->get($temp_id_map);
        
        $data['current_class_fname']=array();            
        $data['current_class_sname']=array();
        $data['current_class_scid']=array();
        $data['current_class_slid']=array();
        $i=0;

        foreach($query->result() as $result1)
        {
            array_push($data['current_class_fname'],$result1->f_name);
            array_push($data['current_class_sname'],$result1->s_name);
            array_push($data['current_class_scid'],$result1->cid);
            array_push($data['current_class_slid'],$result1->lid);
            $i++;
        }
        $data['current_class_member']=$i;

        $this->load->view('teachers/T_header');
        //$this->load->view('students/S_leftpanel');
        $this->load->view('teachers/T_middle_getstudents',$data);
        $this->load->view('teachers/footer');

      }
      else 
      {
        $data['error'] = 'Yor are not logged in';
        redirect(base_url());
      }
    }

    public function teachers_all()
    {

      if($this->is_loggedin())
      {
        $temp_lid=$this->session->userdata('mylid');
        $temp_id_map=$this->session->userdata('myschool_id').'_id_map';
        $temp_class_info_snapshot=$this->session->userdata('myschool_id')."_class_info_snapshot";
        $temp_sub_class_sec=$this->session->userdata('myschool_id')."_sub_class_sec";
        $query=$this->db
                    ->distinct('class_id')
                    ->get($temp_class_info_snapshot);

        $data=array();
        $data['t_class_id']=array();
        $i=0;

        foreach($query->result() as $result1)
        {
           array_push($data['t_class_id'], $result1->class_id);
           $i++;
        }

        $data['num_of_class_id']=$i;
        $c_match=substr($this->session->userdata('mylid'),0,3);

        if(substr($this->session->userdata('mylid'),0,1)=='t')
        {
          $q="(select class_id from $temp_class_info_snapshot where class_teacher='$temp_lid') union (select class_id from $temp_sub_class_sec where teacher_lid='$temp_lid')";

          $query=$this->db->query($q);
          $str="(''";
          foreach($query->result() as $result1)
          {
              $str=$str.",'".$result1->class_id."'";
          }
          $str=$str.")";
          $q="select distinct cid,f_name,s_name,post,B.class_id as classid,B.subject_name as subname from $temp_id_map as A,$temp_sub_class_sec as B where lid like 't%' and A.lid=B.teacher_lid and B.class_id in $str order by cid,classid";
          $query1=$this->db->query($q);
        }
        else
        {
         
          $q="select cid,f_name,s_name,post,B.class_id as classid,B.subject_name as subname from $temp_id_map as A,$temp_sub_class_sec as B,$temp_class_info_snapshot as C where A.lid=B.teacher_lid and B.class_id=C.class_id and B.class_id=substr('$temp_lid',1,3)";
          $query1=$this->db->query($q); 
        }
        $teacher[]=array();
        $max_sub[]=0;
        if( substr($this->session->userdata('mylid'),0,1)=='t' )
        {
          $prev_class='';
          $prev_tchr='';
          $i=-1;
          $j=0;
          foreach($query1->result() as $result1)
          {
              
              $curr_class=$result1->classid;
              $curr_tchr=$result1->cid;
              if($prev_class!=$curr_class || $prev_tchr!=$curr_tchr){

                if($prev_class!='')$max_sub[$i]=$j-1;
                $i++;
                $j=0;
                $teacher[$i]['cid']=$result1->cid;
                $teacher[$i]['name']=$result1->f_name." ".$result1->s_name;
                $teacher[$i]['post']=$result1->post;
                $teacher[$i]['class']=$result1->classid;
              } 
              $max_sub[$i]=0;
              $teacher[$i][$j++]=$result1->subname;
              $prev_class=$curr_class;
              $prev_tchr=$curr_tchr;
          }
          $max=$i;
        }
        else
        {
           $prev='';
           $i=-1;
           $j=0;
           foreach($query1->result() as $result1)
            {
              
              $curr=$result1->cid;
              if($prev!=$curr){
              if($prev!='')$teacher[$i]['max_sub']=$j-1;
              $i++;
              $j=0;
              $teacher[$i]['cid']=$result1->cid;
              $teacher[$i]['name']=$result->f_name." ".$result1->s_name;
              $teacher[$i]['post']=$result1->post;
              $teacher[$i]['class']=$result1->classid;
              }
              $max_sub[$i]=0; 
              $teacher[$i][$j++]=$result1->subname;
              $prev=$curr;
              $max_sub=$max_sub[$i];
            }
            $max=$i;
        } 
        for($i=0;$i<=$max;$i++)
        {
         
           $cid=$teacher[$i]['cid'];
           $class=$teacher[$i]['class'];
           $q="select * from $temp_id_map A,$temp_class_info_snapshot B where B.class_teacher=A.lid and A.cid='$cid' and B.class_id='$class'";
           $query=$this->db->query($q);
            $j=0;
            if($query->num_rows()>0)
            {
               foreach($query->result() as $result1)
                {
                  array_push($teacher[$i]['tempT_class_id'], $result1->class_id);
                  $j++;
                }
            }    
            $teacher[$i]['tempT_classLen']=$j;
        }
        $data['teacher']=$teacher;
        $data['max']=$max;
        $data['max_sub']=$max_sub;
        $this->load->view('teachers/T_header');
       // $this->load->view('students/S_leftpanel');
        $this->load->view('teachers/T_middle_getTeachers',$data);
        $this->load->view('teachers/footer');

      }
      else 
      {
        $data['error'] = 'Yor are not logged in';
        redirect(base_url());
      }
    }
    
    public function getstudentstable($str)
    {
      
      if($this->is_loggedin())
      {
        $temp_id_map=$this->session->userdata('myschool_id').'_id_map';
        
        if( $str == 'All' )
        {
            $query=$this->db
                        ->not_like('lid','t','after')
                        ->get($temp_id_map);
        }
        else
        {
            $query=$this->db
                        ->like('lid',$str,'after')
                        ->get($temp_id_map);
        }
        $data=array();
        $data['current_class_fname']=array();            
        $data['current_class_sname']=array();
        $data['current_class_scid']=array();
        $data['current_class_slid']=array();
        $i=0;

        foreach($query->result() as $result1)
        {
            array_push($data['current_class_fname'],$result1->f_name);
            array_push($data['current_class_sname'],$result1->s_name);
            array_push($data['current_class_scid'],$result1->cid);
            array_push($data['current_class_slid'],$result1->lid);
            $i++;
        }
        $data['current_class_member']=$i;
        $this->load->view('teachers/getstudentstable_view',$data);
      }

      else 
      {
        $data['error'] = 'Yor are not logged in';
        redirect(base_url());
      }
    }

    public function getteacherstable($str)
    {

      if($this->is_loggedin())
      {
        $temp_id_map=$this->session->userdata('myschool_id').'_id_map';
        $temp__sub_class_sec=$this->session->userdata('myschool_id').'_sub_class_sec';
        $temp_class_info_snapshot=$this->session->userdata('myschool_id').'_class_info_snapshot';
        if($str=='All')
        {
          $q="select distinct cid,f_name,s_name,post,B.class_id as classid,B.subject_name as subname from $temp_id_map as A,$temp__sub_class_sec as B where lid like 't%' and A.lid=B.teacher_lid order by cid,classid";
          $query=$this->db->query($q);
        }
        else
        {
          $q="select distinct cid,f_name,s_name,post,B.class_id as classid,B.subject_name as subname from $temp_id_map as A,$temp__sub_class_sec as B where A.lid=B.teacher_lid and B.class_id='$str' order by cid";
          $query=$this->db->query($q);
        }
        $teacher[]=array();
        $max_sub[]=0;
        if( $str == 'All' )
        {
            $prev_class='';
            $prev_tchr='';
            $i=-1;
            $j=0;
            foreach($query->result() as $result)
            {
              
              $curr_class=$result->classid;
              $curr_tchr=$result->cid;
              if($prev_class!=$curr_class || $prev_tchr!=$curr_tchr){
      
                  if($prev_class!='')$max_sub[$i]=$j-1;
                  $i++;
                  $j=0;
                  $teacher[$i]['cid']=$result->cid;
                  $teacher[$i]['name']=$result->f_name." ".$result->s_name;
                  $teacher[$i]['post']=$result->post;
                  $teacher[$i]['class']=$result->classid;
            }
            $max_sub[$i]=0; 
            $teacher[$i][$j++]=$result->subname;
            $prev_tchr=$curr_tchr;
            $prev_class=$curr_class;
            }
            $max=$i;
        }
        else
        {
          $prev='';
          $i=-1;
          $j=0;
          foreach($query->result() as $result)
          {
              $curr=$result->cid;
              if($prev!=$curr){
              if($prev!='')$max_sub[$i]=$j-1;
              $i++;
              $j=0;
              $teacher[$i]['cid']=$result->cid;
              $teacher[$i]['name']=$result->f_name." ".$result->s_name;
              $teacher[$i]['post']=$result->post;
              $teacher[$i]['class']=$result->classid;
              }
              $max_sub[$i]=0; 
              $teacher[$i][$j++]=$result->subname;
              $prev=$curr;
          }
          $max=$i;
        }
        for($i=0;$i<=$max;$i++)
        {
         
           $cid=$teacher[$i]['cid'];
           $class=$teacher[$i]['class'];
           $q="select * from $temp_id_map A,$temp_class_info_snapshot B where B.class_teacher=A.lid and A.cid='$cid' and B.class_id='$class'";
           $query=$this->db->query($q);
            $j=0;
            if($query->num_rows()>0)
            {
               foreach($query->result() as $result1)
                {
                  array_push($teacher[$i]['tempT_class_id'], $result1->class_id);
                  $j++;
                }
            }    
            $teacher[$i]['tempT_classLen']=$j;
        }
        $data['teacher']=$teacher;
        $data['max']=$max;
        $data['max_sub']=$max_sub;
        $this->load->view('teachers/getteacherstable_view',$data);
      }
      else 
      {
        $data['error'] = 'Yor are not logged in';
        redirect(base_url());
      }
    }
    public function anns_Individual()
    {
      if($this->is_loggedin())
      {
        $temp_individual_ann=$this->session->userdata('myschool_id').'_individual_ann';
        $temp_individual_msg=$this->session->userdata('myschool_id').'_individual_msg';
        $temp_id_map=$this->session->userdata('myschool_id').'_id_map';
        $temp_lid=$this->session->userdata('mylid');
        $data=array();
        $highlight['highlight']="Announcements";
        $highlight['image_cid']=$this->session->userdata('cid');
        if(substr($temp_lid,0,1)!='t' && substr($temp_lid,0,1)!='p')
        {
            $query=$this->db
                        ->where('individual_lid',$temp_lid)
                        ->order_by('serial_number','desc')
                        ->get($temp_individual_ann);
            $data['existance']=$query->num_rows();
            if($query->num_rows()>0)
            {  
              $student_ann[]=array();
              $i=0;          
              foreach($query->result() as $result)
              {
                $temp_announcements=$this->session->userdata('myschool_id').'_announcements'.$result->serial_number;
                $temp_anns_string=$result->received_ann_string;
                $i=strlen($temp_anns_string);
                $x=strlen($temp_anns_string);
                $var='';
                $check=0;

                while($i>0)
                {
                  if($temp_anns_string[$i-1]=='1')
                  {
                    if($check==0) {   $check=1;  }
                    else{ $var=$var." or "; }
                    $var=$var."position = '".($x-$i+1)."'";
                  }
                  $i--;
                }

                $query1=$this->db
                            ->where($var)
                            ->order_by('date_time_posted','desc')
                            ->get($temp_announcements);

                foreach($query1->result() as $result1)
                {
                    $student_ann[$i]['ann_id']=$result1->ann_id;
                    $student_ann[$i]['date_time_posted']=$result1->date_time_posted;
                    $query2=$this->db
                                 ->where('lid',$result1->sender_lid)
                                 ->get($temp_id_map);
                    $student_ann[$i]['ann_sender']=$query2->row()->f_name." ".$query2->row()->s_name;
                    $query2=$this->db
                                 ->where('individual_lid',$temp_lid)
                                 ->order_by('serial_number','desc')
                                 ->get($temp_individual_msg);
                    $student_ann[$i]['messageid']=array();
                    $student_ann[$i]['mdate_time_posted']=array();
                    $student_ann[$i]['sender_name']=array();
                    $student_ann[$i]['num_of_msg']=0;
                    foreach($query2->result() as $result2)
                    {
                        $temp_message=$temp_message1=$this->session->userdata('myschool_id').'_message'.$result2->serial_number;
                        $temp_sent_string=$result2->sent_string;
                        $temp_received_string=$result2->received_string;
                        $var="( ";
                        $check=0;

                        $j=strlen($temp_sent_string);
                        $x=strlen($temp_sent_string);

                        while($j>0)
                        {
                          if($temp_sent_string[$j-1]=='1')
                          {
                            if($check==0) {   $check=1;  }
                            else{ $var=$var." or "; }
                            $var=$var."position = '".($x-$j+1)."'";
                          }
                          $j--;
                        }
                      
                        $j=strlen($temp_received_string);
                        $x=strlen($temp_received_string);

                        while($j>0)
                        {
                          if($temp_received_string[$j-1]=='1')
                          {
                            if($check==0) {   $check=1;  }
                            else{ $var=$var." or "; }
                            $var=$var."position = '".($x-$j+1)."'";
                          }
                          $j--;
                        }
                        $var=$var." ) ";
                        $query3=$this->db
                                     ->where($var)
                                     ->where('category',$result1->ann_id)
                                     ->order_by('date_time_posted','asc')
                                     ->get($temp_message);  
                        foreach($query3->result() as $result3)
                        {

                              array_push($student_ann[$i]['messageid'], $result3->msg_id);
                              array_push($student_ann[$i]['mdate_time_posted'],$result3->date_time_posted);
                              $query4=$this->db
                                           ->where('lid',$result3->sender_lid)
                                           ->get($temp_id_map);
                              $result4=$query4->row();
                              array_push($student_ann[$i]['sender_name'],$result4->f_name." ".$result4->s_name);
                        }
                        $student_ann[$i]['num_of_msg']=$student_ann[$i]['num_of_msg']+$query3->num_rows();
                    }     
                  $i++;
                }
              }
              $data['student_ann']=$student_ann;
              $data['num_of_ann']=$i;
            }
        }
        else
        {

            $query=$this->db
                        ->where('individual_lid',$temp_lid)
                        ->order_by('serial_number','desc')
                        ->get($temp_individual_ann);
            $data['existance']=$query->num_rows();
            if($query->num_rows()>0)
            {  
              $student_ann[]=array();
              $i=0;          
              foreach($query->result() as $result)
              {
                $temp_announcements=$this->session->userdata('myschool_id').'_announcements'.$result->serial_number;
                $temp_anns_string=$result->received_ann_string;
                $i=strlen($temp_anns_string);
                $x=strlen($temp_anns_string);
                $var='';
                $check=0;

                while($i>0)
                {
                  if($temp_anns_string[$i-1]=='1')
                  {
                    if($check==0) {   $check=1;  }
                    else{ $var=$var." or "; }
                    $var=$var."position = '".($x-$i+1)."'";
                  }
                  $i--;
                }

                $query1=$this->db
                            ->where($var)
                            ->order_by('date_time_posted','desc')
                            ->get($temp_announcements);

                foreach($query1->result() as $result1)
                {
                    $student_ann[$i]['ann_id']=$result1->ann_id;
                    $student_ann[$i]['date_time_posted']=$result1->date_time_posted;
                    $query2=$this->db
                                 ->where('lid',$result1->sender_lid)
                                 ->get($temp_id_map);

                    $student_ann[$i]['ann_sender']=$query2->row()->f_name." ".$query2->row()->s_name;
                    $query2=$this->db
                                 ->where('individual_lid',$temp_lid)
                                 ->order_by('serial_number','desc')
                                 ->get($temp_individual_msg);
                    $student_ann[$i]['messageid']=array();
                    $student_ann[$i]['mdate_time_posted']=array();
                    $student_ann[$i]['sender_name']=array();

                    foreach($query2->result() as $result2)
                    {
                        $temp_messages=$temp_message1=$this->session->userdata('myschool_id').'_message'.$result2->serial_number;
                        $query3=$this->db
                                     ->where('category',$result1->ann_id)
                                     ->order_by('date_time_posted','asc')
                                     ->get($temp_message);  
                        foreach($query3->result() as $result3)
                        {

                              array_push($student_ann[$i]['messageid'], $result3->msg_id);
                              array_push($student_ann[$i]['mdate_time_posted'],$result3->date_time_posted);
                              $query4=$this->db
                                           ->where('lid',$result3->sender_lid)
                                           ->get($temp_id_map);
                              $result4=$query4->row();
                              array_push($student_ann[$i]['sender_name'],$result4->f_name." ".$result4->s_name);
                        }
                        $student_ann[$i]['num_of_msg']=$student_ann[$i]['num_of_msg']+$query3->num_rows();
                    }     
                  $i++;
                }
              }
              $data['student_ann']=$student_ann;
              $data['num_of_ann']=$i;
            }
        }
        $this->load->view('teachers/T_header');
        $this->load->view('teachers/T_leftpanel',$highlight);
        $this->load->view('teachers/T_announcements',$data);
        $this->load->view('students/footer');
        
      }
      else
      {
        $data['error'] = 'You are not logged in';
        redirect(base_url());
      }
    }
    public function msg_Individual()
    {
      
      if($this->is_loggedin())
      {
        $temp_individual_msg=$this->session->userdata('myschool_id').'_individual_msg';
        $temp_id_map=$this->session->userdata('myschool_id').'_id_map';
        $temp_lid=$this->session->userdata('mylid');
        $data=array();
        $highlight['highlight']="Messages";
        $highlight['image_cid']=$this->session->userdata('cid');
        $query=$this->db
                        ->where('individual_lid',$temp_lid)
                        ->get($temp_individual_msg);
        $data['existance']=$query->num_rows();
        if($query->num_rows()>0)
        {
            $msgthread=array();
            $mthread_group=array();
            foreach($query->result() as $result)
            { 
                $temp_message=$this->session->userdata('myschool_id').'_message'.$result->serial_number;
                $temp_sent_string=$result->sent_string;
                $temp_received_string=$result->received_string;
                $var="( ";
                $check=0;

                $i=strlen($temp_sent_string);
                $x=strlen($temp_sent_string);

                while($i>0)
                {
                  if($temp_sent_string[$i-1]=='1')
                  {
                    if($check==0) {   $check=1;  }
                    else{ $var=$var." or "; }
                    $var=$var."position = '".($x-$i+1)."'";
                  }
                  $i--;
                }
                
                $i=strlen($temp_received_string);
                $x=strlen($temp_received_string);

                while($i>0)
                {
                  if($temp_received_string[$i-1]=='1')
                  {
                    if($check==0) {   $check=1;  }
                    else{ $var=$var." or "; }
                    $var=$var."position = '".($x-$i+1)."'";
                  }
                  $i--;
                }
                $var=$var." ) ";
                $query1=$this->db
                             ->where($var)
                             ->order_by('date_time_posted','desc')
                             ->get($temp_message);
                $data['num_of_sr_msg']=$query1->num_rows();
                if($query1->num_rows()>0)
                {
                      foreach($query1->result() as $result1)
                      {
                        
                        $temp_recever_string=$result1->receiver_string;
                        $var="";
                        $i=strlen($temp_recever_string);
                        $x=strlen($temp_recever_string);
                        while($i>0)
                        {
                          if($temp_recever_string[$i-1]=='1')
                          {
                            $query2=$this->db
                                         ->where('position',$x-$i+1)
                                         ->get($temp_individual_msg);
                            $query3=$this->db
                                         ->where('lid',$query2->row()->individual_lid)
                                         ->get($temp_id_map);
                            $var=$var."-".$query3->row()->f_name;
                          }
                          $i--;
                        }
                        $query2=$this->db
                                     ->where('lid',$result1->sender_lid)
                                     ->get($temp_id_map);
                        $var=$var."-".$query2->row()->f_name;
                        if (!array_key_exists($var, $msgthread))
                        {
                         $msgthread[$var]['messageid']=array();
                         $msgthread[$var]['msender']=array();
                         $msgthread[$var]['posted_date']=array(); 
                        }
                        array_push($mthread_group,$var);
                        array_push($msgthread[$var]['messageid'],$result1->msg_id);
                        array_push($msgthread[$var]['msender'], $query2->row()->f_name." ".$query2->row()->s_name);
                        array_push($msgthread[$var]['posted_date'],$result1->date_time_posted);
                      }
                }
            }
            $data['msgthread']=$msgthread;
            $data['mthread_group']=$mthread_group;
        }
        $this->load->view('teachers/T_header');
        $this->load->view('teachers/T_leftpanel',$highlight);
        $this->load->view('teachers/T_message',$data);
        $this->load->view('teachers/footer');
      } 
      else
      {
        $data['error'] = 'You are not logged in';
        redirect(base_url(),$data);
      }
    }
    public function individual_profile()
    {
      if($this->is_loggedin())
      {
       $str=$this->input->get('profile_cid', TRUE);
       if(empty($str))
        {
          $str=$this->session->userdata('cid');
        }
        $highlight['image_cid']=$str;
        $highlight['highlight']="";
        $temp_id_map=$this->session->userdata('myschool_id').'_id_map';
        $query1=$this->db
                    ->where('lid',$this->session->userdata('mylid'))
                    ->or_where('lid',$this->session->userdata('myr_s_lid_1'))->or_where('lid',$this->session->userdata('myr_s_lid_2'))
                    ->or_where('lid',$this->session->userdata('myr_s_lid_3'))->or_where('lid',$this->session->userdata('myr_s_lid_4'))
                    ->or_where('lid',$this->session->userdata('myr_s_lid_5'))->or_where('lid',$this->session->userdata('myr_s_lid_6'))
                    ->or_where('lid',$this->session->userdata('myr_s_lid_7'))->or_where('lid',$this->session->userdata('myr_s_lid_8'))
                    ->or_where('lid',$this->session->userdata('myr_t_lid_1'))->or_where('lid',$this->session->userdata('myr_t_lid_2'))
                    ->or_where('lid',$this->session->userdata('myr_t_lid_3'))->or_where('lid',$this->session->userdata('myr_t_lid_4'))
                    ->or_where('lid',$this->session->userdata('myr_t_lid_5'))->or_where('lid',$this->session->userdata('myr_t_lid_6'))
                    ->or_where('lid',$this->session->userdata('myr_t_lid_7'))->or_where('lid',$this->session->userdata('myr_t_lid_8'))
                    ->get($temp_id_map);
        $result_length=0;
        $result=array();
        foreach($query1->result() as $result1)
        {
            array_push($result, $result1);
            $result_length++;
        }
        $data['result']=$result;
        $data['result_length']=$result_length;
        $query2=$this->db
                     ->select('lid')
                     ->where('cid',$str)
                     ->get($temp_id_map);
        if(substr($query2->row()->lid,0,1)=='t')
        {
          $temp_personal_info=$this->session->userdata('myschool_id').'_personal_info_teacher';
        }
        else
        {
          $temp_personal_info=$this->session->userdata('myschool_id').'_personal_info_student'; 
        }
        $query=$this->db
                    ->where('cid',$str)
                    ->get($temp_personal_info);
        $data['Profileinfo']=$query->row_array();
        $this->load->view('teachers/T_header');
        $this->load->view('teachers/T_leftpanel',$highlight);
        $this->load->view('teachers/profile_view',$data);
      }
      else
      {
        $data['error']="you are not logged in";
        redirect(base_url(),$data);
      }
    }
  }

?>