<?php
/**
 * This is a controller for functions that are used globally
 * that is, without regard to the type of user
**/
class common extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function is_loggedin()
    {
        return $this->session->userdata('logged_in');
    }

    public function assign_job()
    {
        // Check if already logged in
        if(!$this->is_loggedin())
        {
            $data['error'] = "Yor aren't logged in";
            $this->load->view('pages/index', $data);
        }
        else
        {
            // Table name stings to be used
            /*
            $table_announcements = $this->session->userdata('myschool_id') . '_announcements';
            $table_sub_class_sec = $this->session->userdata('myschool_id') . '_sub_class_sec';
            $table_class_info_snapshot = $this->session->userdata('myschool_id') . '_class_info_snapshot';
            */
            $table_id_map = $this->session->userdata('myschool_id') . '_id_map';
            $table_job_assignment = $this->session->userdata('myschool_id') . '_job_assignment';
            $table_school_id_map = $this->session->userdata('myschool_id') . '_id_map';

            $lid = $this->session->userdata('lid');
            $query = $this->db->where('lid', $lid)->get($table_school_id_map);
            $data['my_l_info'] = $query->row_array();
            $result = $this->db->select('lid')->get($table_id_map);
            $data['users'] = $result->result_array();
            $data['userdata'] = $this->session->all_userdata();
            $data['highlight'] = 'Assign Job';
            $data['variant'] = 'default';

            $this->load->library('form_validation');
            // Check if the form was submitted. If yes, process
            if($this->input->server('REQUEST_METHOD') == 'POST')
            {
                // Validate data
                $this->load->helper('form');

                $this->form_validation->set_rules('message', 'Details of job', 'trim|required|xss_clean');
                $this->form_validation->set_rules('target', 'Date of completion', 'trim|required|xss_clean');

                // Run validation on data
                // If fails, we load the homepage for logging in again
                // Error messages for validation will be displayed above the form
                // TODO : Use set_value() to generate submitted data on user side
                if ($this->form_validation->run() == FALSE)
                {
                    $this->load->view('admin/header', $data);
                    $this->load->view('admin/leftnav', $data);
                    $this->load->view('admin/assign_job_content', $data);
                    $this->load->view('common/footer', $data);
                }

                $message = $this->input->post('message');
                $respondent = $this->input->post('respondent');
                $job_id = $lid . "_" . date('Ymd_His');
                $date_time_completion = $this->input->post('date_time_completion');
                $mail = $this->input->post('mail');
                if($mail == 1)
                {
                    mail('prasoon92.iitr@gmail.com', 'Announcement', $message);
                }
                $date_time_posted = date('Y-m-d');
                $arr = array(
                        'job_id' => $job_id,
                        'details' => $message,
                        'assigned_by_lid' => $lid,
                        'assigned_to_lid' => $respondent,
                        'date_time_update' => $date_time_posted,
                        'date_time_completion' => date('Y-m-d', strtotime($date_time_completion)),
                        'details' => $message,
                        'ticket_update' => '',
                        'initial' => '1'
                    );
                $this->db->insert($table_job_assignment, $arr);
                $data['message'] = 'Job successfully assigned';
            }

            // Now, we output the view
            $this->load->view('admin/header', $data);
            $this->load->view('admin/leftnav', $data);
            $this->load->view('admin/assign_job_content', $data);
            $this->load->view('common/footer', $data);
        }
    }
    // function end: home

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
