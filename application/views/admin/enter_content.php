<div id="middle-part">
    <div id="middlenavistrip" class="wrap">

        <div class="middlemenu">
            <?php
            if(isset($message))
            {
                echo $message;
            }
            ?>
            <table border="1">
                <tr>
                    <th>Serial no.</th>
                    <th>Teacher name:</th>
                </tr>
                <?php foreach($teachers as $key=>$teacher): ?>
                    <tr>
                        <td><?=$key + 1?></td>
                        <td><?=$teacher->f_name?> <?=$teacher->m_name?> <?=$teacher->s_name?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
<form method="post" action="<?=base_url('admin/initial_entry/enter_principal')?>">
<h4>Enter teacher information</h4>
<table>
<tr>
<td>
First name:
</td>
<td>
<input type="text" name="f_name">
</td>
</tr>
<tr>
<tr>
<td>
Middle name:
</td>
<td>
<input type="text" name="m_name">
</td>
</tr>
<tr>
<td>
Last name:
</td>
<td>
<input type="text" name="s_name">
</td>
</tr>

<td>
cid:
</td>
<td>
<input type="text" name="cid">
</td>
</tr>

<td>
lid:
</td>
<td>
<input type="text" name="lid">
</td>
</tr>

<td>
password:
</td>
<td>
<input type="text" name="password">
</td>
</tr>
<td>Gender:
</td>
<td>
<input type="radio" name="gender" value="1">Male
<input type="radio" name="gender" value="0">Female
</td>
</tr>
<tr>
<td>
Dob:
</td>
<td>
<input id="datepicker" name="dob">
</td>
</tr>
<tr>
<td>
Nationality:
</td>
<td>
<input type="text" name="nationality">
</td>
</tr>
<tr>
<td>
Blood group:
</td>
<td>
<input type="text" name="blood_grp">
</td>
</tr>
<tr>
<td>
Father's name:
</td>
<td>
<input type="text" name="father_name">
</td>
</tr>
<tr>
<td>
Mother's name:
</td>
<td>
<input type="text" name="mother_name">
</td>
</tr>
<tr>
<td>
Personal phone:
</td>
<td>
<input type="text" name="phone_p">
</td>
</tr>
<tr>
<td>
Office phone:
</td>
<td>
<input type="text" name="phone_s">
</td>
</tr>
<tr>
<td>
Email:
</td>
<td>
<input type="text" name="email">
</td>
</tr>
<tr>
<td>
Address Line 1:
</td>
<td>
<input type="text" name="address_l1">
</td>
</tr>
<tr>
<td>
Address Line 2(locality):
</td>
<td>
<input type="text" name="address_l2_locality">
</td>
</tr>
<tr>
<td>
Address line 3(city):
</td>
<td>
<input type="text" name="address_l3_city">
</td>
</tr>
<tr>
<td>
Address pincode:
</td>
<td>
<input type="text" name="address_pincode">
</td>
</tr>
<tr>
<td>
Year of admission:
</td>
<td>
<input type="text" name="year_admit">
</td>
</tr>

</table>

<input type="submit" name="submit">
</form>
