<div id="middle-part">
    <div id="middlenavistrip" class="wrap">
        <?php echo validation_errors(); ?>

        <div class="middlemenu">
            <?php
            if(isset($message))
            {
                echo $message;
            }
            ?>
            <form method="post" action="<?=current_url()?>" >
                <select id="std" name="respondent">
                    <option value="">Select a recipient:</option>
                    <?php foreach($users as $user): ?>
                        <option value="<?=$user['lid']?>" ><?=$user['lid']?></option>
                    <?php endforeach; ?>
                </select>
                <br/>
                Details of job:
                <br/>
                <input type="text" name="message"></input>
                <br/>
                <input type="checkbox" value="1" name="mail">Mail</input>
                <input type="checkbox" value="1" name="sms">SMS</input>
                <br/>
                Date of completion:
                <br/>
                <input id="datepicker" name="target"/>
                <br/>
                <input type="submit" name="submit"></name>
            </form>
        </div>
    </div>
</div>
