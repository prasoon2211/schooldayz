<script type="text/javascript">
    function submitform()
    {
        document.forms["myform"].submit();
    }
</script>
<!--
<script type="text/javasctipt" src="<?=base_url('public/js/editablegrid.js')?>"></script>
<link rel="stylesheet" href="<?=base_url('public/css/editablegrid.js')?>" />

<script>
    window.onload = function() {
        editableGrid = new EditableGrid("classGrid"); 
        editableGrid.tableLoaded = function() { this.renderGrid("tablecontent", "testgrid"); };
        editableGrid.loadJSON("grid.json");
    } 
</script>
-->
<script>
// Used jQuery not $ because some idiot had defined a function named $ elsewhere
jQuery(function () {
    jQuery("td.editable").mouseenter(function(){
            //alert("adaf");
            jQuery(this).css('border', '1px solid #999');
        });
    jQuery("td.editable").mouseleave(function(){
            //alert("adaf");
            jQuery(this).css('border', 'none');
        });
    jQuery("td.editable").click(function () {
        var OriginalContent = jQuery(this).text();

        jQuery(this).addClass("cellEditing");
        jQuery(this).html("<input style='width:45px;' type='text' value='" + OriginalContent + "' />");
        jQuery(this).children().first().focus();

        jQuery(this).children().first().keypress(function (e) {
            if (e.which == 13) {
                var newContent = jQuery(this).val();
                if(newContent != OriginalContent)
                {
                    jQuery(this).parent().addClass("contentChanged");
                }
                jQuery(this).parent().html(newContent);
                jQuery(this).parent().removeClass("cellEditing");
            }
        });
    jQuery(this).children().first().blur(function(){
        jQuery(this).parent().text(OriginalContent);
        jQuery(this).parent().removeClass("cellEditing");
    });

    });
});
</script>

<script>
// To hadle with form upload
jQuery(document).ready(function(){
    jQuery("#submitTable").click(function(){
        var rows = jQuery("tr.editableRow");
        var tr, td;
        var cell;
        var className, sectionName;
        var classid, sectionid;
        var post = {};
        var json;
        for(var i = 0; i < rows.length; i++)
        {
            tr = jQuery(rows[i]);
            classid = tr.children(".className").attr("data-clsid");
            if(classid == undefined) classid = "newclass";
            post[classid] = {};
            obj = post[classid];
            td = tr.children(".contentChanged");
            for(var j = 0; j < td.length; j++)
            {
                cell = jQuery(td[j]);
                if(cell.hasClass('className'))
                {
                    className = tr.children(".className").html();
                    obj['className'] = className;
                }
                else
                {
                    sectionid = cell.attr("data-secid");
                    sectionName = cell.html();
                    obj[sectionid] = sectionName;
                }
            }
        }
        json = JSON.stringify(post);

        // Now we post the json string to the server
        // where it will be decoded
        jQuery.ajax({
            type : "POST",
            url : "<?=base_url('admin/enter_class_process')?>",
            data : json
        }).done(function(data) {alert(data); });
    });
});
</script>

<table id="infoTable" width="780" border="0" style="margin-left:4px" align="left" cellpadding="0" cellspacing="0">
        <tr class="bg-blue-color">
            <td width="70" height="30" align="center">Serial No</td>
            <td width="70" height="30" align="center">Class Name</td>
            <td width="70" height="30" align="center">Section 1</td>
            <td width="70" height="30" align="center">Section 2</td>
            <td width="70" height="30" align="center">Section 3</td>
            <td width="70" height="30" align="center">Section 4</td>
            <td width="70" height="30" align="center">Section 5</td>
            <td width="70" height="30" align="center">Section 6</td>
            <td width="70" height="30" align="center">Section 7</td>
            <td width="70" height="30" align="center">Section 8</td>
            <td width="70" height="30" align="center">Section 9</td>
            <td width="70" height="30" align="center">Section 10</td>
        </tr>

        <?php $loop_index = 1; ?>
        <?php foreach($classid_section as $classid=>$section_array): ?>
            <!-- Here loop over every class - gives one table row -->
            <?php if($loop_index%2==0): ?>
                <tr class="editableRow bg-graer-color">
            <?php else: ?>
                <tr class="editableRow bg-white-color">
            <?php endif; ?>
                <td style="width:100px; margin:0px; text-align:center;"class="sn" width="70" height="30" align="center"> <?=$loop_index?> </td>
                <td style="width:100px; margin:0px; text-align:center;" class="className editable" data-clsid="<?=$classid?>" width="70" height="30" align="center"><?=$classid_class_map[$classid]?></td>
                <?php for($i=0; $i <= 9; $i++): ?>
                    <?php if(isset($section_array[$i])): ?>
                        <?php $s = $section_array[$i]; ?>
                        <td style="width:100px; margin:0px; text-align:center;" class="section editable" data-secid="<?=$s['sec_id']?>" width="70" height="30" align="center"><?=$s['sec_name']?></td>
                    <?php else: ?>
                        <td style="width:100px; margin:0px; text-align:center;" class="section editable" data-secid="sec<?=$i+1?>" width="70" height="30" align="center"></td>
                    <?php endif; ?>
                <?php endfor; ?>
                <!--
                <?php foreach($section_array as $index=>$s): ?>
                    <td class="section editable" data-secid="<?=$s['sec_id']?>" width="70" height="30" align="center"><?=$s['sec_name']?></td>
                <?php endforeach; ?>
                -->
            </tr>
            <?php $loop_index++; ?>
        <?php endforeach; ?>

        <tr class="editableRow" name-classid="None">
            <td style="width:100px; margin:0px; text-align:center;" class="sn" width="70" height="30" align="center"><?=$loop_index?></td>
            <td style="width:100px; margin:0px; text-align:center;" class="className editable" width="70" height="30" align="center"></td>
            <?php for($i=1; $i<=10; $i++): ?>
                <td style="width:100px; margin:0px; text-align:center;" class="sec_ editable" data-secid="sec<?=$i?>" width="70" height="30" align="center"></td>
            <?php endfor; ?>
        </tr>
    </table>
    <button id="submitTable">Submit Changes</button>
    <div id="result"></div>
