<div id="middle-part">
<div id="middlenavistrip" class="wrap">
<div class="middlemenu">
<link rel="stylesheet" href="<?=base_url('public/bootstrap/css/bootstrap.min.css')?>" />
<link rel="stylesheet" href="<?=base_url('public/bootstrap/js/bootstrap.min.js')?>" />


<script>
jQuery(document).ready(function(){
    
    var all_selected = {};
    var map = {};
   jQuery('.class-button').click(function(){
      var classid = jQuery(this).attr("data-classid");
      var date = jQuery('#datepicker').val();
      jQuery.ajax({
        url : '<?=base_url('admin/ajax_student_list')?>',
        type : 'post',
        data : { class_id : classid, date : date }
      }).done(function(callback_data){
        jQuery('#list').html(callback_data);
        jQuery('.roll_number').bind('click', function(){
              var roll_number = jQuery(this).html();
              var lid = jQuery(this).attr('data-lid');
              //alert(roll_number);
              if(jQuery(this).hasClass('selected'))
              {
                    jQuery(this).removeClass('selected');
                    jQuery(this).css('background-color', 'green');
                    all_selected[roll_number] = lid;
                    map[lid] = '0';
                    //console.log(all_selected);
              }
              else
              {
                    jQuery(this).addClass('selected');
                    jQuery(this).css('background-color', '#F00');
                    all_selected[roll_number] = lid;
                    map[lid] = '1';
                    //console.log(all_selected);
              }
        });
        jQuery('.att_A').css('background-color', '#ff1111');
        jQuery('.att_P').css('background-color', '#34f11f'); 
      });
      

    });

   jQuery('#submit-data').click(function(){
      var all_roll_numbers = jQuery('#roll_list').val();
      var date = jQuery('#datepicker').val();
      for(var key in all_selected)
      {
          if(all_roll_numbers.indexOf(key) == -1)
          {
              alert('Selected studends and manually filled list do not match');
              return 1;
          }
      }
      var json = JSON.stringify(all_selected);
      var map_json = JSON.stringify(map);
      jQuery.ajax({
         url : '<?=base_url('admin/ajax_attendance_process')?>',
         type : 'post',
         data : {json : json, date : date, map_json : map_json }
        }).done(function(data){
            all_selected = {};
            jQuery('#list').html('');
            jQuery('#roll_list').val('');
            alert(data);
        }); 
   });
    jQuery(function() {
        jQuery( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
    });
});

</script>

<div>
<p>Select a date:</p>
<input type="text" id="datepicker" />
</div>

<table>
        <?php foreach($classid_section as $classid=>$section_array): ?>
            <!-- Here loop over every class - gives one table row -->
            <tr>
                <td class="className" data-clsid="<?=$classid?>" width="70" height="30" align="center"><?=$classid_class_map[$classid]?></td>
                <?php foreach($section_array as $s): ?>
                        <td class="section" width="70" height="30" align="center"><button class="class-button" style="min-width:70px;" data-classid="<?=$classid.$s['sec_id']?>XX"><?=$s['sec_name']?></button></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
</table>

<div id="list">

</div>

<div>
<p>Enter roll numbers you have changes manually separated by commas.</p>
<input type="text" id="roll_list" />
<button id="submit-data">Submit</button>
</div>

<?php /*
<form method="post" action="<?=$_SERVER['PHP_SELF']?>">

<select name="class" onchange="showSections(this.value)">
<option value="">Select/ a class:</option>
<?php foreach($result as $row)
{
	$class=substr($row['class'],0,2);
?>
	<option value="<?=$row['class']?>"><?=$row['class_name']?></option>
<?php } ?>
</select>

<select id="sec" name="section" onchange="showRollnos(this.value)">
<option value="">Select a section:</option>
</select>
Date:
<input id="datepicker" name="date" onchange="changeColor(this.value)"/>
<br/>

<div id="classes">
<table>
<?php $prev=''; ?>
<tr>
<?php foreach($result1 as $row)
{
	$curr=substr($row['class_id'],0,2);
	if($prev!=$curr)
	{
?>
		</tr><tr>
		<td><?=$row['class_name']?></td>
<?php	} ?>
	<td id="<?=$row['class_id']?>"><?=$row['section_name']?></td>
	<?php $prev=$curr;
}
?>
</tr>
<tr></tr>
<tr></tr>
</table>
</div>

<table>
<tr>
<td>
<div id="rollno">
</div>
</td>
</tr>
</table>

<input id="submit" type="submit" name="submit" disabled="true" />

</form>
		
		<?php echo $this->session->userdata('date') .$this->session->userdata('last_update') . $this->session->userdata('session_start'); ?>
		<br/>
        <?php
		//length=$length
		//diff=$diff1,$diff
        ?>
		
		<?php //create a string as 00.........1 which is used to update the values of class_open and attendance_taken in sjca_class_info_snapshot ?>
		<?php
        $str='';
		for($i=1;$i<$length;$i++)
		{
			$str=$str.'0';
		}
		$str=$str.'1';

		$new_class_open=($this->session->userdata("class_open") | $str);
		$new_atten_taken=($this->session->userdata("attendance_taken") | $str);
		$new_actual_class=$new_class_open&$new_atten_taken;
		$total_class=substr_count($new_actual_class,'1');

			//update the table sjca_id_map for the students of that class		
			$sql="select * from $tbl3_name where lid like '".$section."%'";
			$result=mysql_query($sql);
			$YTD_class=0;
			$n_stds=0;
			$today_present=0;
			$today_absent=0;
			while( $row=mysql_fetch_array($result) )
			{
				$attendance=$row['attendance'];
				$var="atten".substr($row['lid'],3,2);
				$str='';
				//if previous date is selected, the string str contains 000...1 with no of 0s equal to the position of that date in the bit string
				//in this case , $str is ORed with the attendance string
				//else it will contain no of zeros equal to difference of last update and date selected
				//in this case $str is concatenated to the attendance string
				if($diff1>0)$len=$diff1;
				else $len=strlen($attendance)+$diff1;
				for($i=1;$i<$len;$i++)$str=$str.'0';
				//echo $len."<br/>";
				if( $_POST[$row['lid']] == 1 )
				{
					$str=$str.'1';
					$today_present++;
				}
				if( $_POST[$row['lid']] == 0 )
				{
					$str=$str.'0';
					$today_absent++;
				}
				echo $str."<br/>";
				if($diff1>0)$str=$row['attendance'].$str;
				else $str=( $row['attendance'] | $str );
				echo $str."<br/>"."<br/>";
				$total_present=substr_count($str,'1');
				$YTD=round($total_present*100/$total_class,2);
				$YTD_class=$YTD_class+$YTD;
				$n_stds++;
				$sql="update $tbl3_name set attendance='$str' where lid='".$row['lid']."'";
				echo $sql;
				mysql_query($sql)or die("error in executing sql statement");
			}
			$YTD_class=$YTD_class/$n_stds;	
			
			//if user selects a previous date difference will be negative
			//in this case we have dont have to update last attendance_update
			
			if($diff1>0)
				$sql="update $tbl5_name set attendance_string='$new_class_open',attendance_taken='$new_atten_taken',last_attendance_update=str_to_date('".$this->session->userdata("date")."','%m/%d/%Y'),YTD_class=$YTD_class,today_present=$today_present,today_absent=$today_absent where class_id='".$section."'";
			else
				$sql="update $tbl5_name set attendance_string='$new_class_open',attendance_taken='$new_atten_taken',YTD_class='$YTD_class' where class_id='".$section."'";
			echo $sql;	
			mysql_query($sql)or die("error in executing sql statement");
			//<script>changeColor('$section');</script>
			
}			

</div>
</div>
</div>

    <!--START footer-->
    <div id="main-footer">
      <div class="footer">
        <h1>School Dayz</h1>
        <ul>
          <li><a href="#">About Us</a> |</li>
          <li><a href="#">Press & Blogs</a> |</li>
          <li><a href="#">Copyright</a> |</li>
          <li><a href="$">Creators & Partners</a> |</li>
          <li><a href="#">Privacy</a> |</li>
          <li><a href="#">Safety</a> |</li>
          <li><a href="#">Report a bug</a></li>
        </ul>
      </div>
    </div>
    <!--end footer-->
    
  </div>
  <!--end main-->
</div>
 </div>
</div>
</body>
</html>

*/?>
