<div id="main-right">
  <!--announcements start-->
  <div id="main-right-messages">
    <div id="main-right-messages-text"><strong>Welcome <?php echo $this->session->userdata('myf_name');?></strong><br />
            You have 3 new Announcements, 1 new Messages and 2 new Results
    </div>
    <div id="announcements">
      <h1>Announcements:</h1>

<!--get the messages received by him
//records from sjca_messages in which he is one of the respondents-->
            <?php 
            $numofann=$num_of_ann_msg[0];
            $x=0;
            while($x<$numofann):
            ?>
            <div id="announcements-row" style="border-top:1px solid #bfbfbf;"> 
              <a href="#">
              <table width="470" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr style="font-weight:bold">
                  <td width="26" align="center" valign="middle" style="background:url(<?php echo base_url('public/images/main.png');?>) no-repeat -101px 4px">
                  <img src="<?php echo base_url('public/images/main-tran.png');?>" alt="" width="14" height="14" /></td>
                  
                  <td width="100" align="left" valign="top"><?php echo $t_ann_sender[$x];?></td>

                  <td width="250" align="left" valign="top">
                    <?php 
                        $file=file_get_contents('./public/msgDatabase/announcement/'.$t_announcement[$x].'.txt');
                        echo substr($file,0,35).'...';
                    ?>
                  </td>
                  <td width="30" align="center" valign="middle" class="attach">
                  <img src="".base_url('public/images/main-tran.png')."" alt="" width="15" height="7" /></td>
                  <td width="74" align="left" valign="top"><?php echo $t_ann_time[$x];?></td>
                </tr>
              </table>
              </a>
            </div>
            <?php $x++;endwhile;?> 
    </div>
    <div id="announcements">
      <h1>Messages:</h1>
            
            <?php 
            $numofmsg=$num_of_ann_msg[1];
            $x=0;
            while($x<$numofmsg):
            ?>
            <div id="announcements-row" style="border-top:1px solid #bfbfbf;"> 
              <a href="#">
              <table width="470" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr style="font-weight:bold">
                  <td width="26" align="center" valign="middle" style="background:url(<?php echo base_url('public/images/main.png');?>) no-repeat -101px 4px">
                  <img src="<?php echo base_url('public/images/main-tran.png');?>" alt="" width="14" height="14" /></td>
                  
                  <td width="100" align="left" valign="top"><?php echo $t_msg_sender[$x];?></td>

                  <td width="250" align="left" valign="top">
                    <?php 
                        $file=file_get_contents('./public/msgDatabase/message/'.$t_message[$x].'.txt');
                        echo substr($file,0,35).'...';
                    ?>
                  </td>
                  <td width="30" align="center" valign="middle" class="attach">
                  <img src="".base_url('public/images/main-tran.png')."" alt="" width="15" height="7" /></td>
                  <td width="74" align="left" valign="top"><?php echo $t_msg_time[$x];?></td>
                </tr>
              </table>
              </a>
            </div>
            <?php $x++;endwhile;?> 
    </div>
  </div> 
       <!--announcements end-->
<!--user start-->
  <div id="main-right-user-box" style="margin-top:0px;">
    <div  id="main-right-user-day">
      <a href="<?php echo base_url('teacher_home/student_all');?>">Classmates</a>
      <a href="<?php echo base_url('teacher_home/student_all');?>">All Classes</a>
    </div>
    <table width="318" height="212" border="0" align="left" cellpadding="1" cellspacing="1" style="margin:0 0 2px 2px; ">
      <tr>  
        <?php
          $num_all = $num_of_ann_msg[2];
          $x = 0;
          $carriage_return=0;
          while($x<$num_all):
        ?>
        <?php 
            if(substr($result[$x]->lid,0,1)<>"t" && $carriage_return<6):
              $carriage_return=$carriage_return+1;
            //Displaying image here
                if($carriage_return==4):
        ?>    
      </tr><tr>

        <?php 
            endif;
            if($result[$x]->lid==$this->session->userdata('mylid')):
              $my_l_info=$result[$x];
              $carriage_return=$carriage_return-1;
            else:
        ?>
          <td width="103" height="103" align="" valign="top">
            <div class="user-photo instructor  custom-photo" style="width:103px; height:103px; float:left; margin-right:3px;">
              <a href="<?=base_url('teacher_home/individual_profile/?profile_cid='.$result[$x]->cid);?>" style="margin-top:0px; width:100px;"> 
                <img src="<?php echo base_url('public/images/'.$result[$x]->cid.'_icon.gif');?>" alt="" width="103" height="103"/>
                  <span class="table-text-box">
                    <div style="-webkit-linear-gradient(transparent, rgba(0, 0, 0, .7));color: #fff;display: block;width: 100%;word-wrap: break-word;">
                    <div style="margin: 0px">  
                    <?php echo $result[$x]->f_name." ".$result[$x]->s_name;?>
                    </div>
                    </div>
                </span>
                </a>
            </div>                
            <!--a href="profile_student.php?profile_lid=<?php echo $this->session->userdata('mylid');?>">
              <img src="<?php echo base_url('public/images/'.$result[$x]->cid.'_icon.gif');?>" alt="" style="width:103px; height:103px;">
            </a-->
          </td>
        <?php
              endif;
              endif;
             $x++;
            endwhile;
        ?>       
        </tr>
      </table>
      <div  id="main-right-user-day">
        <a href="<?php echo base_url('teacher_home/teachers_all');?>">Teachers</a>
        <a href="<?php echo base_url('teacher_home/teachers_all');?>">All</a>
      </div>
      <table width="318" height="212" border="0" align="left" cellpadding="1" cellspacing="1" style="margin:0 0 2px 2px;">
        <tr>
          <?php
          //extracting and displaying teachers details
            $num_all = $num_of_ann_msg[2];
            $x = 0;
            $carriage_return=0;
            while($x<$num_all):
          ?>
          <?php 
              if(substr($result[$x]->lid,0,1)=="t" && $carriage_return<6):
                $carriage_return=$carriage_return+1;
              //Displaying image here
                  if($carriage_return==4):
          ?>    
                </tr><tr>

          <?php 
              endif;
              if($result[$x]->lid==$this->session->userdata('mylid')):
                $my_l_info=$result[$x];
                $carriage_return=$carriage_return-1;
              else:
          ?>
            <td width="103" height="103" align="" valign="top">
            <div class="user-photo instructor  custom-photo" style="width:103px; height:103px; float:left; margin-right:3px;">
              <a href="<?=base_url('teacher_home/individual_profile/?profile_cid='.$result[$x]->cid);?>" style="margin-top:0px; width:100px;"> 
                <img src="<?php echo base_url('public/images/'.$result[$x]->cid.'_icon.gif');?>" alt="" width="103" height="103"/>
                  <span class="table-text-box">
                     <div style="-webkit-linear-gradient(transparent, rgba(0, 0, 0, .7));color: #fff;display: block;width: 100%;word-wrap: break-word;">
                    <div style="margin: 0px">  
                    <?php echo $result[$x]->f_name." ".$result[$x]->s_name;?>
                    </div>
                    </div>
                </span>
                </a>
            </div>                
            <!--a href="profile_student.php?profile_lid=<?php echo $this->session->userdata('mylid');?>">
              <img src="<?php echo base_url('public/images/'.$result[$x]->cid.'_icon.gif');?>" alt="" style="width:103px; height:103px;">
            </a-->
          </td>
         <?php
          endif;
          endif;
          $x++;
        endwhile;
          ?>    
        </tr>
      </table>
    </div>
       <!--user end-->
</div>
