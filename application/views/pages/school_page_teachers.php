<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Information for Coachings</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url("public/css/schooldayz.css");?>" media="all">
<link rel="stylesheet" href="<?php echo base_url("public/css/font.css");?>">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Advent+Pro:500,700|Noticia+Text:400,400italic,700,700italic&amp;subset=latin,latin-ext">
<!--banner start-->
<script type="text/javascript" async="" src="<?php echo base_url("public/banner/ga.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("public/banner/query.js"); ?>"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url("public/banner/jquery.cycle.all.2.72.js"); ?>"></script>
<script type="text/javascript">
$(function() {
    $('#slideshow').cycle({
        speed:       1000,
        timeout:     3000,
        pager:      '#portfolio-dots',
        pagerEvent: 'mouseover'
    });
	$('#slideshow1').cycle({
        speed:       500,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
	$('#slideshow2').cycle({
        speed:       700,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
	$('#slideshow3').cycle({
        speed:       1000,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
});
</script>
<script type="text/javascript" language="javascript">    
 $(document).ready(function()    
 	 {       
	  $(document).bind("contextmenu",function(e){               
	  return false;       
	  });    
	 });  </script>
<!--end-->
</head>

<body style="background:url(<?php echo base_url("public/images/masthead.png"); ?>) repeat-x -470px 0px;">
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-39151982-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
<div id="wapper-top-1">
  <div id="top-header-1">
    <div id="header-1">
      <div class="logo-1"> <a href="<?php echo base_url('staticpages/pages');?>"><img src="<?php echo base_url("public/images/lore.png");?>" alt="" width="82" height="88" border="0" align="left"></a>
        <label>School Dayz</label>
      </div>
    </div>
    <div id="navigation-1">
      <ul>
        <ul>
        <li><a href="<?php echo base_url('staticpages/pages');?>"><span>HOME</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/school_page");?>"><span>MANAGEMENT</span></a></li>
        <li class="active"><a href="<?php echo base_url("staticpages/pages/school_page_teachers");?>"><span>TEACHERS</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/school_page_students");?>"><span>STUDENTS</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/school_page_parents");?>"><span>PARENTS</span></a></li>
      </ul>
      </ul>
    </div>
  </div>
</div>

<div id="wapper-middle-1">
   	<div class="banner-bg-1">
		<div class="sliderbg">
       		<div id="slideshow" style="position: relative;">
           	<div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
           		<img src="<?php echo base_url('public/images/banner.png');?>" alt=""></div>
           	<div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
           		<img src="<?php echo base_url('public/images/banner-1.png');?>" alt=""></div>
           	<div style="position: absolute; top: 0px; left: 0px; display: block; z-index: 5; opacity: 1; width: 971px; height: 293px;">
           		<img src="<?php echo base_url('public/images/banner-2.png');?>" alt=""></div>
           	<div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
           		<img src="<?php echo base_url('public/images/banner-3.png');?>" alt=""></div>
         	</div>
         	<div id="portfolio-top">
           	<div id="portfolio-dots"><a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
           		<a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
           		<a href="http://www.schooldayz.co.in/coaching_page.html#" class="activeSlide">&nbsp;&nbsp;</a>
           		<a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
           	</div>
        	</div>
		</div>
	</div>
	
	<div class="content-1">
		<div class="content-left">
			<img src="<?php echo base_url('public/images/spt1.jpg');?>" alt="" width="370" height="270" align="left" /><span></span>
			<div class="content-box-1">
        	<h1>Manage your efforts in the right direction</h1>
			</br>You know the best about teaching and we just ensure that you don’t have to put your time in operational or unproductive work and simply get time to teach and guide your students. We help you to know each and every student more closely so that you can guide them at your best.
			</div>
		</div>

		<div class="content-left">
			<div class="content-box-2">
			<h1>New tools to guide and educate your students</h1>
			</br>Schooldayz methodical learning and transformational Guidance gives teachers insight into the learning process, specifically in terms of efficacy, engagement, and knowledge retention. The platform also provides an unprecedented flexibility of scope; teachers can grasp patterns in student activity and performance across the whole class or look into individual student profiles to determine exactly why a student is struggling.
			</div>
			<img src="<?php echo base_url('public/images/spt2.jpg');?>" alt="" width="370" height="315" align="right" /><span></span>
		</div>

		<div class="content-left">
			<img src="<?php echo base_url('public/images/spt3.png'); ?>" alt="" width="370" height="250" align="left" /><span></span>
			<div class="content-box-1">
        	<h1>Know bit by bit development of your students</h1>
			</br>We micro analyze the performance of individual students over a period of time in each area of learning to provide a clear picture to the teacher about development over a period of time.
			</div>
		</div>
		
		<div class="content-left">
			<div class="content-box-2">
			<h1>Access all information from a single location</h1>
			</br>Teachers can access all information related to Notices, Announcements, latest Coaching events, duties and students’ analysis from a single location on our platform leading to instant connectivity and updated information.
			</div>
			<img src="<?php echo base_url('public/images/spt4.jpg');?>" alt="" width="370" height="300" align="right" /><span></span>
		</div>

		<div class="content-left"><img src="<?php echo base_url('public/images/spt5.jpg');?>" alt="" width="370" height="300" align="left" /><span></span>
			<div class="content-box-1">
        	<h1>Easier Syllabus, Classwork and Homework Management</h1>
			</br>Our Platform helps you manage your syllabus completion and classwork & homework Management with much more ease. Now it is easier to manage your work and focus on the core activity of teaching and guiding your students 
			</div>
		</div>
		
		<div class="content-left">
			<div class="content-box-2">
			<h1>Addressing the Diverse Needs of Students</h1>
			</br>One of the biggest challenges facing teachers and Coaching administrators today is the growing diversity of the students within their population. A greater diversity of students means a greater diversity of needs to consider.Few Student struggle in specific subject and sub areas; others have difficulty with focus or organization, some may be particularly weak in visualization but may possess unusual strengths in another.</br></br>Schooldayz methodical learning & Transformational guidance allows teachers to address the needs of diverse students while gaining insight into the learning process. The platform may discover, for instance, that a student who is weak in English is struggling because he has difficulty with reading Comprehension; teacher in that case can guide and work with the student on vocabulary and grammar in specific. Later, the teacher may be informed that another student who understands mathematical concepts but has trouble with carelessness in arithmetic should receive feedback about how to develop stronger estimation abilities or check work once completed. The instructor can then coach that student with a precise understanding of his particular weaknesses.</br></br>For example, the Schooldayz result dashboard measure student progress through the course. This concept functions as an indicator that helps teachers efficiently grasp information about the class as a whole. Using this tool and others, teachers can see reporting data from two perspectives:</br></br>The Whole(x) Class:</br>The Schooldayz Result dashboard includes a histogram which provides a big picture assessment of the whole class’ “track” status.</br>Using the dashboard, teachers can also see how students are performing in individual subject areas; which segments of material are the most and least challenging for students; and what kinds of patterns in both performance and activity emerge across the class. After multiple section of teaching the same course, teachers will be able to compare data from section to section; Schooldayz analytics will help teachers with useful information and interpretation of the results.</br></br>Individual Students:</br>While the reporting dashboard in Schooldayz is streamlined so that teachers can focus on the big picture, the dashboard also allows teachers to click into the interface and look into each student’s work in the system. Teachers can see how students have performed on specific quizzes and exams. If a student isn’t grasping the material, teachers can determine using analytics that guide them to specific data points, where precisely any misunderstanding is occurring.</br>This capacity allows teachers to both address the diverse needs of students and better understand their content, so that they can refine it over a period of time.
			</div>
			<img src="<?php echo base_url('public/images/spt6.jpg');?>" alt="" width="370" height="315" align="right" /><span></span>
		</div>

		<div class="content-left"><img src="<?php echo base_url('public/images/spt7.jpg');?>" alt="" width="370" height="280" align="left" /><span></span>
			<div class="content-box-1">
        	<h1>Learning and results at its best</h1>
			</br>Schooldayz through its Methodical learning and Transformational Guidance ensures that each individual is able to unlock the hidden potential within and realize the true power of Education. Development in Education is the prerequisite for the development of a society and humanity. Together we can spread highest level of learning bring the best out of every individual.
			</div>
		</div>

		<div class="content-left">
			<div class="content-box-2">
			<h1>Easier communication and connectivity with parents</h1>
			</br>We facilitate instant connectivity between Parents and Teachers which ensure proper flow of instructions & guidance between them leading to better care and development of the Child.   
			</div>
			<img src="<?php echo base_url('public/images/spt8.jpg');?>" alt="" width="370" height="315" align="right" /><span></span>
		</div>
				
	</div>			
    
	<div id="main-footer-1">
    	<div id="footer-1">
        	<ul>
				<li style="background:url(<?php  echo base_url('public/images/twitter.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">TWITTER</a></li>
				<li style="background:url(<?php  echo base_url('public/images/forrest.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">FACEBOOK</a></li>
				<li style="background:url(<?php  echo base_url('public/images/google=.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">GOOGLE+</a></li><li style="background:url(<?php  echo base_url('public/images/flickr.png');?>) no-repeat 0px 5px;"><a href="school_page.html#">YOUTUBE</a></li>
			</ul>
        	<h2>COPYRIGHT © 2012 SCHOOLDAYZ. ALL RIGHTS RESERVED.</h2>
        </div>
    </div>
</div>
</body>
</html>
