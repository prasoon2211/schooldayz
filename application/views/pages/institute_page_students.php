<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Information for Coachings</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url("public/css/schooldayz.css");?>" media="all">
<link rel="stylesheet" href="<?php echo base_url("public/css/font.css");?>">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Advent+Pro:500,700|Noticia+Text:400,400italic,700,700italic&amp;subset=latin,latin-ext">
<!--banner start-->
<script type="text/javascript" async="" src="<?php echo base_url("public/banner/ga.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("public/banner/query.js"); ?>"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url("public/banner/jquery.cycle.all.2.72.js"); ?>"></script>
<script type="text/javascript">
$(function() {
    $('#slideshow').cycle({
        speed:       1000,
        timeout:     3000,
        pager:      '#portfolio-dots',
        pagerEvent: 'mouseover'
    });
  $('#slideshow1').cycle({
        speed:       500,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
  $('#slideshow2').cycle({
        speed:       700,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
  $('#slideshow3').cycle({
        speed:       1000,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
});
</script>
<script type="text/javascript" language="javascript">    
 $(document).ready(function()    
   {       
    $(document).bind("contextmenu",function(e){               
    return false;       
    });    
   });  </script>
<!--end-->
</head>

<body style="background:url(<?php echo base_url("public/images/masthead.png"); ?>) repeat-x -470px 0px;">
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-39151982-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
<div id="wapper-top-1">
  <div id="top-header-1">
    <div id="header-1">
      <div class="logo-1"> <a href="<?php echo base_url('staticpages/pages');?>"><img src="<?php echo base_url("public/images/lore.png");?>" alt="" width="82" height="88" border="0" align="left"></a>
        <label>School Dayz</label>
      </div>
    </div>
    <div id="navigation-1">
      <ul>
        <ul>
        <li><a href="<?php echo base_url('staticpages/pages');?>"><span>HOME</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/institute_page");?>"><span>MANAGEMENT</span></a></li>
        <li class="active"><a href="<?php echo base_url("staticpages/pages/institute_page_teachers");?>"><span>TEACHERS</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/institute_page_students");?>"><span>STUDENTS</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/institute_page_parents");?>"><span>PARENTS</span></a></li>
      </ul>
      </ul>
      </ul>
    </div>
  </div>
</div>

<div id="wapper-middle-1">
    <div class="banner-bg-1">
    <div class="sliderbg">
          <div id="slideshow" style="position: relative;">
            <div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
              <img src="<?php echo base_url('public/images/banner.png');?>" alt=""></div>
            <div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
              <img src="<?php echo base_url('public/images/banner-1.png');?>" alt=""></div>
            <div style="position: absolute; top: 0px; left: 0px; display: block; z-index: 5; opacity: 1; width: 971px; height: 293px;">
              <img src="<?php echo base_url('public/images/banner-2.png');?>" alt=""></div>
            <div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
              <img src="<?php echo base_url('public/images/banner-3.png');?>" alt=""></div>
          </div>
          <div id="portfolio-top">
            <div id="portfolio-dots"><a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
              <a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
              <a href="http://www.schooldayz.co.in/coaching_page.html#" class="activeSlide">&nbsp;&nbsp;</a>
              <a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
            </div>
          </div>
    </div>
  </div>
  
  <div class="content-1">
    <div class="content-left">
      <img src="<?php echo base_url('public/images/sps1.jpg');?>" alt="" width="370" height="270" align="left" /><span></span>
      <div class="content-box-1">
          <h1>Now sharing and learning more fun and rewarding</h1>
      </br>At Schooldayz we believe together we can achieve the next level of evolution in education. Even most advance Economics theories have explained that what we can achieve working together is beyond imagination of an individual. Schooldayz provides an excellent platform to learn from your classmates as well share your knowledge.
      </div>
    </div>

    <div class="content-left">
      <div class="content-box-2">
      <h1> Know yourself even better</h1>
      </br>We have seen that over centuries proper guidance has provided a significant progress in individuals and helped them to achieve their dreams in life. We provide you a guide & a friend who know you the best and sometimes even better than you. Our tools help you analyze yourself like never before, we tell you specific areas to work and guide you to overcome your difficulties. Students may sometimes develop blind spots which they can’t analyze but Schooldayz helps you understand and improve your performance and take it to the next level.
      </div>
      <img src="<?php echo base_url('public/images/sps2.jpg');?>" alt="" width="370" height="420" align="right" /><span></span>
    </div>

    <div class="content-left"><img src="<?php echo base_url('public/images/sps3.jpg');?>" alt="" width="370" height="250" align="left" /><span></span>
      <div class="content-box-1">
          <h1>Get your doubts and problems solved in minutes</h1>
      </br>You can discuss your problems and area of improvements real time from your teachers as well as your fellow students & seniors and get a solution in just no time.</br>Guidance at right time is the true secret behind success & development. Schooldayz methodical learning & transformational guidance can improve student engagement by increasing self-confidence and encouraging productive learning habits.</br></br>Instant Feedback</br>Students are less likely to lose focus if feedback is immediate and they can quickly self-correct. A continuously transformational guiding system is able to deliver personalized feedback to both multiple choice and free response questions quickly — that is, instantaneously or near instantaneously. The result is pacing that is conducive to risk-taking, experimentation, iterative development, and rapid learning.</br></br>Community & Collaboration:</br>Schooldayz can improve student engagement by weaving a collaborative study component into coursework. Schooldayz , for instance, provides a facility that allows teachers to group students who are working on the same material together. Teachers can also arrange peer review opportunities and form groups of students whose abilities complement each other.
      </div>
    </div>
        
  </div>      
    
  <div id="main-footer-1">
      <div id="footer-1">
          <ul>
        <li style="background:url(<?php  echo base_url('public/images/twitter.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">TWITTER</a></li>
        <li style="background:url(<?php  echo base_url('public/images/forrest.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">FACEBOOK</a></li>
        <li style="background:url(<?php  echo base_url('public/images/google=.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">GOOGLE+</a></li><li style="background:url(<?php  echo base_url('public/images/flickr.png');?>) no-repeat 0px 5px;"><a href="school_page.html#">YOUTUBE</a></li>
      </ul>
          <h2>COPYRIGHT © 2012 SCHOOLDAYZ. ALL RIGHTS RESERVED.</h2>
        </div>
    </div>
</div>
</body>
</html>
