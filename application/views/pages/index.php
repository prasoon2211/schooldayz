<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>School Dayz</title> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/social.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/style.css'); ?>" media="all"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/style-1.css'); ?>" media="all"/>
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Advent+Pro:500,700|Noticia+Text:400,400italic,700,700italic&amp;subset=latin,latin-ext">
</head><body><script type="text/javascript">var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-39151982-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	<div id="wrapper">
		<div id="intWrapper">
			<div id="intHeader">
				<div id="intlogo">School Dayz</div>
				<div id="intHeadRight">
				<div><a href="<?php echo base_url('staticpages/pages/contact_us'); ?>">Contact Us</a></div>
				<div><a href="<?php echo base_url('staticpages/pages/career');?>">Career</a></div>
				<div><a href="<?php echo base_url('staticpages/pages/company_profile');?>">Company Profile</a></div>
				</div></div>
				<div id="intCont"><div id="intContL">
					<h1>Next Level of Evolution in Education</h1>
					<p>World's most powerful platform to learn, share knowledge and experience revolutionary development.</p></div>
					<script type="text/javascript">function submitform()
	{
	  document.forms["myform"].submit();
	}
	</script>
	<form id="myform" method="post" action="<?php echo base_url('auth/login'); ?>">
		<div id="intContR"><h2>Sign in</h2>
			<h3><img src="<?php echo base_url('public/images/facebook.gif'); ?>" alt=""></h3>
			<ul><li>Name</li> 
				<li><input name="myusername" type="text" id="myusername" class="inputBox"></li> 
				<li>Password</li> 
				<li><input name="mypassword" type="password" id="mypassword" class="inputBox"></li> 
				<li style="height:30px;margin-top:10px;">
					<div><div id="box-result-one-buttons">
					<!--          <input name="" type="image" src="images/btn2.png" style="float:left; width:80px" onclick=\"submitform()\"> -->
					<a href="javascript: submitform()">
						<button type="button" class=" xyz-result xyz-result-primary">Sign In</button> </a>
						<!--		<button href="calendar.html" type="button" class=" xyz-result xyz-result-primary" onClick=";window.location.href=this.getAttribute(&#39;href&#39;);return false;" role="button"><span><span class="yt-valign-trick"></span></span><span class="xyz-result-content">Sign in</span></button> --></div>
					</div>
					<input name="input2" type="checkbox" value="" style="float:left;margin-top:-12px;margin-left:90px; width:15px;"> 
					<span style="float:left; margin-top:-12px;margin-left:10px;font-size:11px;">Keep me logged in</span></li> 
					<li>
						<a href="index.html#" style="color:#CCC;text-decoration:none;font-size:11px;margin-top:10px;">Forgot your password?</a>
					</li>
				</ul></div>
			</form></div>
			<div id="intServ">
				<div class="intServBox-1">
					<a href="<?php echo base_url('staticpages/pages/school_page'); ?>">
						<div class="intServBox" style="margin-left:0px">
							<div class="intServBoxImg">
								<img src="<?php echo base_url('public/images/box-img2.png'); ?>" width="210" height="120" style="padding-left:50px;padding-top:10px;"></div>
								<div class="intServBoxTxt">For Schools</div>
							</div>
						</a></div>
						<div class="intServBox-1">
							<a href="<?php echo base_url('staticpages/pages/coaching_page');?>">
								<div class="intServBox">
									<div class="intServBoxImg">
										<img src="<?php echo base_url('public/images/box-img1.png'); ?>" width="210" height="120" style="padding-left:50px;padding-top:10px;"></div>
										<div class="intServBoxTxt">For Coachings</div></div></a></div>
										<div class="intServBox-1">
											<a href="<?php echo base_url('staticpages/pages/institute_page');?>">
												<div class="intServBox" style="margin-right:0px">
													<div class="intServBoxImg">
														<img src="<?php echo base_url('public/images/lecturer.png'); ?>" style="padding-left:100px;padding-top:10px;"></div>
														<div class="intServBoxTxt">For Higher Education</div></div>
													</a></div>
												</div></div>
											</div>
										</body>
										</html>
