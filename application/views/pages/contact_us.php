<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contact Us</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/schooldayz.css'); ?>" media="all" />
<link rel="stylesheet" href="<?php echo base_url('public/css/font.css');?>">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Advent+Pro:500,700|Noticia+Text:400,400italic,700,700italic&amp;subset=latin,latin-ext">
<!--banner start-->
<script type="text/javascript" src="<?php echo base_url('public/banner/query.js');?>"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url('public/banner/jquery.cycle.all.2.72.js');?>">
</script>
<script type="text/javascript">
$(function() {
    $('#slideshow').cycle({
        speed:       1000,
        timeout:     3000,
        pager:      '#portfolio-dots',
        pagerEvent: 'mouseover'
    });
	$('#slideshow1').cycle({
        speed:       500,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
	$('#slideshow2').cycle({
        speed:       700,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
	$('#slideshow3').cycle({
        speed:       1000,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
});
</script>
<script type="text/javascript" language="javascript">    
 $(document).ready(function()    
 	 {       
	  $(document).bind("contextmenu",function(e){               
	  return false;       
	  });    
	 });  </script>
<!--end-->
</head>

<body style="background:url(<?php echo base_url('public/images/masthead.png');?>) repeat-x -470px 0px;">
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-39151982-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
<div id="wapper-top-1">
  <div id="top-header-1">
    <div id="header-1">
      <div class="logo-1"> <a href="<?php echo base_url('staticpages/pages');?>"><img src="<?php echo base_url('public/images/lore.png');?>" alt="" width="82" height="88" border="0" align="left" /></a>
        <label>School Dayz</label>
      </div>
    </div>
    <div id="navigation-1">
      <ul>
        <ul>
          <li><a href="<?php echo base_url('staticpages/pages');?>"><span>HOME</span></a></li>
          <li><a href="<?php echo base_url("staticpages/pages/company_profile");?>"><span>COMPANY</span></a></li>
          <li class="active"><a href="<?php echo base_url("staticpages/pages/career");?>"><span>CAREER</span></a></li>
          <li><a href="<?php echo base_url("staticpages/pages/contact_us");?>"><span>CONTACT</span></a></li>
          <li><a href="#"><span>SERVICES</span></a></li>
      </ul>
      </ul>
      </ul>
    </div>
  </div>
</div>

<div id="wapper-middle-1">
   	<div class="banner-bg-1">
		<div class="sliderbg">
       		<div id="slideshow">
           	<div><img src="<?php echo base_url('public/images/banner.png'); ?>" alt="" /></div>
            <div><img src="<?php echo base_url('public/images/banner-1.png'); ?>" alt="" /></div>
            <div><img src="<?php echo base_url('public/images/banner-2.png'); ?>" alt="" /></div>
            <div><img src="<?php echo base_url('public/images/banner-3.png'); ?>" alt="" /></div>
         	</div>
         	<div id="portfolio-top">
           	<div id="portfolio-dots"></div>
        	</div>
		</div>
	</div>
	
	<div class="content-1">
		<div class="content-left">
			<img src="<?php echo base_url('public/images/contact_us1.jpg');?>" alt="" width="370" height="260" align="left" /><span></span>
			<div class="content-box-1">
        	<h1>Contact Us</h1>
			</br>Thank you for visiting Schooldayz online.</br>Post your query here or write to admin@schooldayz.com</br></br>You can contact us at our following offices:</br></br>New Delhi Area:</br>Synapse Technologies India (P) Ltd.<br>M7/4 DLF Phase 2</br>Gurgaon, India 122001</br>Phone: +91-124-4386772</br></br>Mumbai:</br>Synapse Technologies India (P) Ltd.<br>C-806 Green Woods</br>Chakala, Andheri (East)</br>Mumbai, India 400057</br></br>

			</div>
		</div>
	</div>			
    
	<div id="main-footer-1">
    	<div id="footer-1">
        	<ul>
				<li style="background:url(<?php  echo base_url('public/images/twitter.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">TWITTER</a></li>
        <li style="background:url(<?php  echo base_url('public/images/forrest.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">FACEBOOK</a></li>
        <li style="background:url(<?php  echo base_url('public/images/google=.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">GOOGLE+</a></li><li style="background:url(<?php  echo base_url('public/images/flickr.png');?>) no-repeat 0px 5px;"><a href="school_page.html#">YOUTUBE</a></li>
			</ul>
        	<h2>COPYRIGHT © 2012 SCHOOLDAYZ. ALL RIGHTS RESERVED.</h2>
        </div>
    </div>
</div>
</body>
</html>
