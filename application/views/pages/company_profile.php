<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Company Profile</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/schooldayz.css'); ?>" media="all" />
<link rel="stylesheet" href="<?php echo base_url('public/css/font.css');?>">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Advent+Pro:500,700|Noticia+Text:400,400italic,700,700italic&amp;subset=latin,latin-ext">
<!--banner start-->
<script type="text/javascript" src="<?php echo base_url('public/banner/query.js');?>"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url('public/banner/jquery.cycle.all.2.72.js');?>">
</script>
<script type="text/javascript">
$(function() {
    $('#slideshow').cycle({
        speed:       1000,
        timeout:     3000,
        pager:      '#portfolio-dots',
        pagerEvent: 'mouseover'
    });
  $('#slideshow1').cycle({
        speed:       500,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
  $('#slideshow2').cycle({
        speed:       700,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
  $('#slideshow3').cycle({
        speed:       1000,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
});
</script>
<script type="text/javascript" language="javascript">    
 $(document).ready(function()    
   {       
    $(document).bind("contextmenu",function(e){               
    return false;       
    });    
   });  </script>
<!--end-->
</head>

<body style="background:url(<?php echo base_url('public/images/masthead.png');?>) repeat-x -470px 0px;">
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-39151982-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
<div id="wapper-top-1">
  <div id="top-header-1">
    <div id="header-1">
      <div class="logo-1"> <a href="<?php echo base_url('staticpages/pages');?>"><img src="<?php echo base_url('public/images/lore.png');?>" alt="" width="82" height="88" border="0" align="left" /></a>
        <label>School Dayz</label>
      </div>
    </div>
    <div id="navigation-1">
      <ul>
        <ul>
        <li><a href="<?php echo base_url('staticpages/pages');?>"><span>HOME</span></a></li>
          <li><a href="<?php echo base_url("staticpages/pages/company_profile");?>"><span>COMPANY</span></a></li>
          <li class="active"><a href="<?php echo base_url("staticpages/pages/career");?>"><span>CAREER</span></a></li>
          <li><a href="<?php echo base_url("staticpages/pages/contact_us");?>"><span>CONTACT</span></a></li>
          <li><a href="#"><span>SERVICES</span></a></li>
      </ul>
      </ul>
      </ul>
    </div>
  </div>
</div>

<div id="wapper-middle-1">
    <div class="banner-bg-1">
    <div class="sliderbg">
          <div id="slideshow">
            <div><img src="<?php echo base_url('public/images/banner.png'); ?>" alt="" /></div>
            <div><img src="<?php echo base_url('public/images/banner-1.png'); ?>" alt="" /></div>
            <div><img src="<?php echo base_url('public/images/banner-2.png'); ?>" alt="" /></div>
            <div><img src="<?php echo base_url('public/images/banner-3.png'); ?>" alt="" /></div>
          </div>
          <div id="portfolio-top">
            <div id="portfolio-dots"></div>
          </div>
    </div>
  </div>
  
  <div class="content-1">
    <div class="content-left">
      <img src="<?php echo base_url('public/images/about_us_evolution.jpg');?>" alt="" width="370" height="400" align="left" /><span></span>
      <div class="content-box-1">
          <h1>Next Level of Evolution in Education</h1>
      </br>Education is the base of human evolution and when we look back at history; all the great moments that defined the human race were about education. Education is the integral part of development and growth of a civilization as well as a Nation.</br>At Schooldayz we service to take education to the next level of evolution which is by offering personalized and customized educational environment to each and every individual.</br>Schooldayz work to improve every aspect of the education experience. From universities to school to professional training and fully online learning environments, we have the technology and expertise to help you and your students to succeed.
      </div>
    </div>

    <div class="content-left">
      <div class="content-box-2">
      <h1>Technology in growth of Education</h1>
      </br>We attempt to identify the area of improvement in each individual by using latest technologies and most advance statistical and psychological methods which provide enormous advantage for development and learning.</br>Schooldayz is one of the leading innovation & technology driven company revolutionizing the education system around the world. we apply a unique combination of  latest technology and development in the education sector to create ground breaking products for the education sector that make learning and teaching easier, more fun, and more effective.
      </div>
      <img src="<?php echo base_url('public/images/about_us_technology_in_education.jpg');?>" alt="" width="370" height="315" align="right" /><span></span>
    </div>

    <div class="content-left">
      <img src="<?php echo base_url('public/images/about_us_personalized_learning.jpg');?>" alt="" width="370" height="330" align="left" /><span></span>
      <div class="content-box-1">
          <h1>Personalized Learning & Transformational Guidance</h1>
      </br>We are passionate to make education more effective and interactive and constantly research to create an atmosphere of easier and profound learning through our products that account to make teaching a pleasurable experience for teachers as well as students. Each of our products are created with participation and opinion of leading academicians, specialists, researchers around the globe as well as designed using contemporary learning design principles, making it the finest learning methodology.</br>At SchoolDayz, we have created ground-breaking products that are transforming the lives of students studying in our partner schools around the globe. We have products that cater to schools, Colleges, tutorial and Institutions.</br>We develop and market products to strengthen the educations system around the world.</br>
      </div>
    </div>
  </div>      
    
  <div id="main-footer-1">
      <div id="footer-1">
          <ul>
        <li style="background:url(<?php  echo base_url('public/images/twitter.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">TWITTER</a></li>
        <li style="background:url(<?php  echo base_url('public/images/forrest.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">FACEBOOK</a></li>
        <li style="background:url(<?php  echo base_url('public/images/google=.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">GOOGLE+</a></li><li style="background:url(<?php  echo base_url('public/images/flickr.png');?>) no-repeat 0px 5px;"><a href="school_page.html#">YOUTUBE</a></li>
      </ul>
          <h2>COPYRIGHT © 2012 SCHOOLDAYZ. ALL RIGHTS RESERVED.</h2>
        </div>
    </div>
</div>
</body>
</html>
