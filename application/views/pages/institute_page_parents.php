<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Information for Coachings</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url("public/css/schooldayz.css");?>" media="all">
<link rel="stylesheet" href="<?php echo base_url("public/css/font.css");?>">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Advent+Pro:500,700|Noticia+Text:400,400italic,700,700italic&amp;subset=latin,latin-ext">
<!--banner start-->
<script type="text/javascript" async="" src="<?php echo base_url("public/banner/ga.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("public/banner/query.js"); ?>"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url("public/banner/jquery.cycle.all.2.72.js"); ?>"></script>
<script type="text/javascript">
$(function() {
    $('#slideshow').cycle({
        speed:       1000,
        timeout:     3000,
        pager:      '#portfolio-dots',
        pagerEvent: 'mouseover'
    });
  $('#slideshow1').cycle({
        speed:       500,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
  $('#slideshow2').cycle({
        speed:       700,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
  $('#slideshow3').cycle({
        speed:       1000,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
});
</script>
<script type="text/javascript" language="javascript">    
 $(document).ready(function()    
   {       
    $(document).bind("contextmenu",function(e){               
    return false;       
    });    
   });  </script>
<!--end-->
</head>

<body style="background:url(<?php echo base_url("public/images/masthead.png"); ?>) repeat-x -470px 0px;">
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-39151982-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
<div id="wapper-top-1">
  <div id="top-header-1">
    <div id="header-1">
      <div class="logo-1"> <a href="<?php echo base_url('staticpages/pages');?>"><img src="<?php echo base_url("public/images/lore.png");?>" alt="" width="82" height="88" border="0" align="left"></a>
        <label>School Dayz</label>
      </div>
    </div>
    <div id="navigation-1">
      <ul>
        <ul>
        <li><a href="<?php echo base_url('staticpages/pages');?>"><span>HOME</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/institute_page");?>"><span>MANAGEMENT</span></a></li>
        <li class="active"><a href="<?php echo base_url("staticpages/pages/institute_page_teachers");?>"><span>TEACHERS</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/institute_page_students");?>"><span>STUDENTS</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/institute_page_parents");?>"><span>PARENTS</span></a></li>
      </ul>
      </ul>
      </ul>
    </div>
  </div>
</div>

<div id="wapper-middle-1">
    <div class="banner-bg-1">
    <div class="sliderbg">
          <div id="slideshow" style="position: relative;">
            <div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
              <img src="<?php echo base_url('public/images/banner.png');?>" alt=""></div>
            <div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
              <img src="<?php echo base_url('public/images/banner-1.png');?>" alt=""></div>
            <div style="position: absolute; top: 0px; left: 0px; display: block; z-index: 5; opacity: 1; width: 971px; height: 293px;">
              <img src="<?php echo base_url('public/images/banner-2.png');?>" alt=""></div>
            <div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
              <img src="<?php echo base_url('public/images/banner-3.png');?>" alt=""></div>
          </div>
          <div id="portfolio-top">
            <div id="portfolio-dots"><a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
              <a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
              <a href="http://www.schooldayz.co.in/coaching_page.html#" class="activeSlide">&nbsp;&nbsp;</a>
              <a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
            </div>
          </div>
    </div>
  </div>
  
  <div class="content-1">
    <div class="content-left">
      <img src="<?php echo base_url('public/images/spp1.jpg');?>" alt="" width="370" height="380" align="left" /><span></span>
      <div class="content-box-1">
          <h1>See your child achieve the impossible</h1>
      </br>Our only wish as a parent is to see our child accomplish every goal of his or her life. Schooldayz is the next level of evolution in education which with its methodical learning and transformational guidance ensures that your child is under proper supervision at the time when right direction can lead him achieve the goals of his life.
      </div>
    </div>

    <div class="content-left">
      <div class="content-box-2">
      <h1>Easily communicate with Teachers & Coaching to guide your Child</h1>
      </br>You understand & care your child more than anyone in the world. To extend your care and guidance we facilitate instant connectivity with Schools and Teachers to ensure that every care and guidance is taken by the Coaching and teachers as well for the development of your child.
      </div>
      <img src="<?php echo base_url('public/images/spp2.jpg');?>" alt="" width="370" height="300" align="right" /><span></span>
    </div>

    <div class="content-left"><img src="<?php echo base_url('public/images/spp3.jpg');?>" alt="" width="370" height="280" align="left" /><span></span>
      <div class="content-box-1">
          <h1>Tools to scientifically analyze the learning and performance of your child</h1>
      </br>Schooldayz with its scientific methodical learning and transformational guidance closely monitor the learning and performance of your child in every aspect. Suggest the best possible ways to improve and develop. We analyze the conceptual understanding of the student in each and every subject and topic and compare it with other students to recommend the most important are of improvements.
      </div>
    </div>
    
    <div class="content-left">
      <div class="content-box-2">
      <h1>Access Coaching Activities & Child records at a single location</h1>
      </br>Now it is very simple to access Coaching Notice, Announcement, upcoming activities, examination schedule and all other Coaching activities at Schooldayz platform anywhere-anytime. Get complete information on topics taught in class, homework deadlines, upcoming test, syllabus completion, Coaching calendar and many more on just one click.</br></br></br>Daily SMS Alerts on attendance and important activities for easier accessibility and information flow to parents.
      </div>
      <img src="<?php echo base_url('public/images/spp4.jpg');?>" alt="" width="370" height="320" align="right" /><span></span>
    </div>
        
  </div>      
    
  <div id="main-footer-1">
      <div id="footer-1">
          <ul>
        <li style="background:url(<?php  echo base_url('public/images/twitter.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">TWITTER</a></li>
        <li style="background:url(<?php  echo base_url('public/images/forrest.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">FACEBOOK</a></li>
        <li style="background:url(<?php  echo base_url('public/images/google=.png');?>) no-repeat 0px 0px;"><a href="school_page.html#">GOOGLE+</a></li><li style="background:url(<?php  echo base_url('public/images/flickr.png');?>) no-repeat 0px 5px;"><a href="school_page.html#">YOUTUBE</a></li>
      </ul>
          <h2>COPYRIGHT © 2012 SCHOOLDAYZ. ALL RIGHTS RESERVED.</h2>
        </div>
    </div>
</div>
</body>
</html>
