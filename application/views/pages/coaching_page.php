<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Information for Coachings</title>

<link rel="stylesheet" type="text/css" href="<?php echo base_url("public/css/schooldayz.css");?>" media="all">
<link rel="stylesheet" href="<?php echo base_url("public/css/font.css");?>">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Advent+Pro:500,700|Noticia+Text:400,400italic,700,700italic&amp;subset=latin,latin-ext">
<!--banner start-->
<script type="text/javascript" async="" src="<?php echo base_url("public/banner/ga.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("public/banner/query.js"); ?>"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url("public/banner/jquery.cycle.all.2.72.js"); ?>"></script>
<script type="text/javascript">
$(function() {
    $('#slideshow').cycle({
        speed:       1000,
        timeout:     3000,
        pager:      '#portfolio-dots',
        pagerEvent: 'mouseover'
    });
	$('#slideshow1').cycle({
        speed:       500,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
	$('#slideshow2').cycle({
        speed:       700,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
	$('#slideshow3').cycle({
        speed:       1000,
        timeout:     3000,
        //pager:      '#portfolio-dots',
        //pagerEvent: 'mouseover'
    });
});
</script>
<script type="text/javascript" language="javascript">    
 $(document).ready(function()    
 	 {       
	  $(document).bind("contextmenu",function(e){               
	  return false;       
	  });    
	 });  </script>
<!--end-->
</head>

<body style="background:url(<?php echo base_url("public/images/masthead.png"); ?>) repeat-x -470px 0px;">
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-39151982-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
<div id="wapper-top-1">
  <div id="top-header-1">
    <div id="header-1">
      <div class="logo-1"> <a href="<?php echo base_url("staticpages/pages");?>">
      	<img src="<?php echo base_url("public/images/lore.png");?>" alt="" width="82" height="88" border="0" align="left"></a>
        <label>School Dayz</label>
      </div>
    </div>
    <div id="navigation-1">
      <ul>
        <ul>
        <li><a href="<?php echo base_url("staticpages/pages");?>"><span>HOME</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/coaching_page");?>"><span>MANAGEMENT</span></a></li>
        <li class="active"><a href="<?php echo base_url("staticpages/pages/coaching_page_teachers");?>"><span>TEACHERS</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/coaching_page_students");?>"><span>STUDENTS</span></a></li>
        <li><a href="<?php echo base_url("staticpages/pages/coaching_page_parents");?>"><span>PARENTS</span></a></li>
      </ul>
      </ul>
    </div>
  </div>
</div>

<div id="wapper-middle-1">
   	<div class="banner-bg-1">
		<div class="sliderbg">
       		<div id="slideshow" style="position: relative;">
           	<div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
           		<img src="<?php echo base_url('public/images/banner.png');?>" alt=""></div>
           	<div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
           		<img src="<?php echo base_url('public/images/banner-1.png');?>" alt=""></div>
           	<div style="position: absolute; top: 0px; left: 0px; display: block; z-index: 5; opacity: 1; width: 971px; height: 293px;">
           		<img src="<?php echo base_url('public/images/banner-2.png');?>" alt=""></div>
           	<div style="position: absolute; top: 0px; left: 0px; display: none; z-index: 4; opacity: 0; width: 971px; height: 293px;">
           		<img src="<?php echo base_url('public/images/banner-3.png');?>" alt=""></div>
         	</div>
         	<div id="portfolio-top">
           	<div id="portfolio-dots"><a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
           		<a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
           		<a href="http://www.schooldayz.co.in/coaching_page.html#" class="activeSlide">&nbsp;&nbsp;</a>
           		<a href="http://www.schooldayz.co.in/coaching_page.html#" class="">&nbsp;&nbsp;</a>
           	</div>
        	</div>
		</div>
	</div>
	
	<div class="content-1">
		<div class="content-left">
			<img src="<?php echo base_url("public/images/school_page_1.jpg"); ?>" alt="" width="370" height="350" align="left"><span></span>
			<div class="content-box-1">
        	<h1>Methodical learning &amp; Transformational Guidance</h1>
			<br>A method premised on the idea that micro analyzing and guiding individuals can make all the difference in learning &amp; development we wish to spread. The world's most powerful platform to learn, share your knowledge, get transformational guidance and experience revolutionary development.<br>We are continually working to refine and improve our innovative learning technology. Our goal is to work with teachers and institutions to provide every student with a learning experience that is both highly effective and engaging. We want not only to help students meet specific learning objectives – but also provide them with a greater understanding of the transformative power of education, the simple joy of learning something new.
			</div>
		</div>

		<div class="content-left">
			<div class="content-box-2">
			<h1>Make your Coaching No.1 in all parameters</h1>
			<br>Schooldayz through its scientific methodology and micro analysis help the coaching to become No.1 in all the parameters from academics to extracurricular, from results to learning of every individual.<br>We are catering to the next level of evolution in education and learning.
			</div>
			<img src="<?php echo base_url("public/images/coaching_page_2.jpeg");?>" alt="" width="370" height="315" align="right"><span></span>
		</div>

		<div class="content-left">
			<img src="<?php echo base_url("public/images/coaching_page_3.jpeg");?>" alt="" width="370" height="350" align="left"><span></span>
			<div class="content-box-1">
        	<h1>Result analysis through OMR (Optical Mark Recognition)</h1>
			<br>Result analysis like never before, we help you conduct all your tests through the world class OMR Technology used by advance examining bodies. OMR Technology helps us present the analysis at question level. At class level we can analyze the questions which were less attended even after being easy.	Mirco analyze the performance of an individual in a test as well in the subject over a period of time. Statistical suggestions to each and every student to improve his or her marks and conceptual understanding. Our coaching will be able to offer best results with the help of these analyses as well as able to measure area of improvement for each student.
			</div>
		</div>

		<div class="content-left">
			<div class="content-box-2">
			<h1>Most Effective online advertising to specific target audience</h1>
			<br>Schooldayz offers the most effective medium of advertisement and Brand building though its tie ups with World largest online media company like google, youtube, facebook and many other which help us to advertise to people of specific area and age group hence most effectively advertise to your specific target audience.
			</div>
			<img src="<?php echo base_url("public/images/coaching_page_4.jpeg");?>" alt="" width="370" height="315" align="right"><span></span>
		</div>
		
		<div class="content-left">
			<img src="<?php echo base_url("public/images/coaching_page_5.jpeg");?>" alt="" width="370" height="330" align="left"><span></span>
			<div class="content-box-1">
        	<h1>Know statistical tool to better guide your coaching functioning</h1>
			<br>Something that can be measured can be developed and improved; we measure the key performance parameters and together develop solutions to make every aspect of coaching functioning perfect and enriching for the students.
			</div>
		</div>
		
		<div class="content-left">
			<div class="content-box-2">
			<h1>Administration &amp; Educational development like never before</h1>
			<br>We present you an overall Administrative &amp; Educational development of your institute by enhancing every aspect of Administration and Education.<br><br>- Integrate and automate all your processes<br>- Connect with parents and other stakeholders<br>- Track key metrics and ensure fast decision-making<br>- coaching level Real time Analysis for Result , Syllabus completion, Attendance and Classwork/Home work Management<br>- Teacher wise Subject Level Result Analysis and Syllabus completion<br>- Students level Result Analysis, Syllabus completion, Attendance behavior and Classwork/Homework Management<br>- Parents Guidance and Communication measurement at coaching level
			</div>
			<img src="<?php echo base_url("public/images/coaching_page_6.jpeg");?>" alt="" width="370" height="360" align="right"><span></span>
		</div>

		<div class="content-left"><img src="<?php echo base_url("public/images/coaching_page_7.jpeg");?>" alt="" width="370" height="250" align="left"><span></span>
			<div class="content-box-1">
        	<h1>Increase efficiency with better resource utilization</h1>
			<br>We help the coaching and teachers to put their energies in right direction, help them focus on their core activity of teaching and guiding the students and get away from Administrative and paper work.
			</div>
		</div>
		
	</div>			
    
	<div id="main-footer-1">
    	<div id="footer-1">
        	<ul>
				<li style="background:url(<?php echo base_url("public/images/twitter.png"); ?>) no-repeat 0px 0px;"><a href="http://www.schooldayz.co.in/coaching_page.html#">TWITTER</a></li>
				<li style="background:url(<?php echo base_url("public/images/forrest.png"); ?>) no-repeat 0px 0px;"><a href="http://www.schooldayz.co.in/coaching_page.html#">FACEBOOK</a></li>
				<li style="background:url(<?php echo base_url("public/images/google=.png"); ?>) no-repeat 0px 0px;"><a href="http://www.schooldayz.co.in/coaching_page.html#">GOOGLE+</a></li><li style="background:url(images/flickr.png) no-repeat 0px 5px;"><a href="http://www.schooldayz.co.in/coaching_page.html#">YOUTUBE</a></li>
			</ul>
        	<h2>COPYRIGHT © 2012 SCHOOLDAYZ. ALL RIGHTS RESERVED.</h2>
        </div>
    </div>
</div>


</body></html>
