<div id="main-right">
  <!--right part of profile star-->
  <div id="main-right-messages">
    <!--profile description-->
    <div id="announcements">
            <?php if(count($Profileinfo)>0):?>
            <table width="458" border="0" align="left" cellpadding="0" cellspacing="5" style="border:1px solid #dbddde; background:#f2f2f2; margin-left:10px;">
              <tbody><tr>
                <td height="30"><strong>Student Detail</strong></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="135" height="30">Name</td>
                <td width="302"><?=$Profileinfo['f_name']." ".$Profileinfo['m_name']." ".$Profileinfo['s_name'];?></td>
              </tr>
              <tr>
                <td height="30">Father Name</td>
                <td><?=$Profileinfo['father_name'];?></td>
              </tr>
              <tr>
                <td height="35">Mother Name</td>
                <td><?=$Profileinfo['mother_name'];?></td>
              </tr>
              <tr>
                <td height="27">Nationlity</td>
                <td><?=$Profileinfo['nationality'];?></td>
              </tr>
              <tr>
                <td height="30">Blood Group</td>
                <td><?=$Profileinfo['blood_group'];?></td>
              </tr>
              <tr>
                <td height="28">Landline Number</td>
                <td><?=$Profileinfo['phone_p'];?></td>
              </tr>
              <tr>
                <td height="28">Mobile Number</td>
                <td><?=$Profileinfo['phone_s'];?></td>
              </tr>
              <tr>
                <td height="28">Email id</td>
                <td><?=$Profileinfo['email'];?></td>
              </tr>
              <tr>
                <td height="29">Address</td>
                <td><?=$Profileinfo['address_l1']." ".$Profileinfo['address_l2_locality']." ".$Profileinfo['address_l3_city'];?></td>
              </tr>
              <tr>
                <td height="29">Pincode</td>
                <td><?=$Profileinfo['address_pincode']?></td>
              </tr>
              <tr>
                <td height="31">Admission Year</td>
                <td><?=$Profileinfo['year_admit']?></td>
              </tr>
            </tbody></table>
          <?php endif;?>
          </div>
  </div> 
<!--user start-->
  <div id="main-right-user-box" style="margin-top:0px;">
    <div  id="main-right-user-day">
      <a href="<?php echo base_url('student_home/student_all');?>">Classmates</a>
      <a href="<?php echo base_url('student_home/student_all');?>">All Classes</a>
    </div>
    <table width="318" height="212" border="0" align="left" cellpadding="1" cellspacing="1" style="margin:0 0 2px 2px; ">
      <tr>  
        <?php
          $num_all = $result_length;
          $x = 0;
          $carriage_return=0;
          while($x<$num_all):
        ?>
        <?php 
            if(substr($result[$x]->lid,0,1)<>"t" && $carriage_return<6):
              $carriage_return=$carriage_return+1;
            //Displaying image here
                if($carriage_return==4):
        ?>    
      </tr><tr>

        <?php 
            endif;
            if($result[$x]->lid==$this->session->userdata('mylid')):
              $my_l_info=$result[$x];
              $carriage_return=$carriage_return-1;
            else:
        ?>
          <td width="103" height="103" align="" valign="top">
            <div class="user-photo instructor  custom-photo" style="width:103px; height:103px; float:left; margin-right:3px;">
              <a href="<?=base_url('student_home/individual_profile/?profile_cid='.$result[$x]->cid);?>" style="margin-top:0px; width:100px;"> 
                <img src="<?php echo base_url('public/images/'.$result[$x]->cid.'_icon.gif');?>" alt="" width="103" height="103"/>
                  <span class="table-text-box">
                    <div style="-webkit-linear-gradient(transparent, rgba(0, 0, 0, .7));color: #fff;display: block;width: 100%;word-wrap: break-word;">
                    <div style="margin: 0px">  
                    <?php echo $result[$x]->f_name." ".$result[$x]->s_name;?>
                    </div>
                    </div>
                </span>
                </a>
            </div>                
            <!--a href="profile_student.php?profile_lid=<?php echo $this->session->userdata('mylid');?>">
              <img src="<?php echo base_url('public/images/'.$result[$x]->cid.'_icon.gif');?>" alt="" style="width:103px; height:103px;">
            </a-->
          </td>
        <?php
              endif;
              endif;
             $x++;
            endwhile;
        ?>       
        </tr>
      </table>
      <div  id="main-right-user-day">
        <a href="<?php echo base_url('student_home/teachers_all');?>">Teachers</a>
        <a href="<?php echo base_url('student_home/teachers_all');?>">All</a>
      </div>
      <table width="318" height="212" border="0" align="left" cellpadding="1" cellspacing="1" style="margin:0 0 2px 2px;">
        <tr>
          <?php
          //extracting and displaying teachers details
            $num_all = $result_length;
            $x = 0;
            $carriage_return=0;
            while($x<$num_all):
          ?>
          <?php 
              if(substr($result[$x]->lid,0,1)=="t" && $carriage_return<6):
                $carriage_return=$carriage_return+1;
              //Displaying image here
                  if($carriage_return==4):
          ?>    
                </tr><tr>

          <?php 
              endif;
              if($result[$x]->lid==$this->session->userdata('mylid')):
                $my_l_info=$result[$x];
                $carriage_return=$carriage_return-1;
              else:
          ?>
            <td width="103" height="103" align="" valign="top">
            <div class="user-photo instructor  custom-photo" style="width:103px; height:103px; float:left; margin-right:3px;">
              <a href="<?=base_url('student_home/individual_profile/?profile_cid='.$result[$x]->cid);?>" style="margin-top:0px; width:100px;"> 
                <img src="<?php echo base_url('public/images/'.$result[$x]->cid.'_icon.gif');?>" alt="" width="103" height="103"/>
                  <span class="table-text-box">
                     <div style="-webkit-linear-gradient(transparent, rgba(0, 0, 0, .7));color: #fff;display: block;width: 100%;word-wrap: break-word;">
                    <div style="margin: 0px">  
                    <?php echo $result[$x]->f_name." ".$result[$x]->s_name;?>
                    </div>
                    </div>
                </span>
                </a>
            </div>                
            <!--a href="profile_student.php?profile_lid=<?php echo $this->session->userdata('mylid');?>">
              <img src="<?php echo base_url('public/images/'.$result[$x]->cid.'_icon.gif');?>" alt="" style="width:103px; height:103px;">
            </a-->
          </td>
         <?php
          endif;
          endif;
          $x++;
        endwhile;
          ?>    
        </tr>
      </table>
    </div>
       <!--user end-->
</div>
